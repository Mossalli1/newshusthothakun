import { persistReducer, persistStore } from 'redux-persist';
import { createLogger } from 'redux-logger';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import ReduxThunk from 'redux-thunk';
import storage from 'redux-persist/lib/storage';
//
import auth from './reducers/authReducer';
import loading from './reducers/loading';
import user from './reducers/user'
import common from './reducers/common'

const persistConfig = {
  key: 'root',
  storage,
}

const rootReducer = combineReducers({
  auth,
  user,
  common,
  loading,
})

const PersistReducer = persistReducer(persistConfig, rootReducer);

const middlewares = [
  ReduxThunk,
  createLogger(),
];

export const store = createStore(
  PersistReducer,
  applyMiddleware(...middlewares),
)

export const persistor = persistStore(store)
