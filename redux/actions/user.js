import { createAction } from 'redux-act';
import _ from 'lodash';
//
import { KEYS } from '../../utils/constants';
import { setLoading } from './loading';
import { GET_PROFILE, GET_MEDICINE, GET_FEEDBACK, POST_FEEDBACK, GET_PRESCRIPTION, GET_REPORT, POST_REPORT, POST_PRESCRIPTION } from '../../utils/api';
import { getMemberId } from '../../utils/appUtils';
//
export const fetchSuccess = createAction('fetchSuccess');
export const clearUser = createAction('clearUser');


export const fetchProfile = () => {
 return async (dispatch, getState) => {
  const key = KEYS.profile
  const member_id = getMemberId(getState());
   try {
    dispatch(setLoading({ key, value: true }));
    //01010101 
    const res = await fetch(`${GET_PROFILE}member_id=${member_id}`).then(res => res.json());
    // const res = await fetch(`${GET_PROFILE}member_id=${userId}`).then(res => res.json());
    // console.log('PRINT IN %s=====>','Profile', res);
     dispatch(fetchSuccess({ key, value: res[0] }));
   } catch (err) {
    // console.error('PRINT IN %s=====>','Profile', err);
   } finally {
    dispatch(setLoading({ key, value: false }));
   }
 }
}
const med = [
  {
    "id": 1,
    "medicine": "NAPA",
    "med_eat_time": "05:44 AM\n 08:18 PM\n 11:34 PM",
    "duration": 20,
    "due_duration": 7
  },
  {
    "id": 2,
    "medicine": "Anziloc",
    "med_eat_time": "02:42 AM\n 01:05 PM\n 11:35 PM",
    "duration": 10,
    "due_duration": 5
  },
  {
    "id": 3,
    "medicine": "Bumetanide(Bumex)",
    "med_eat_time": "09:00 AM\n 01:10 PM\n 10:10 PM",
    "duration": 20,
    "due_duration": 9
  },
  {
    "id": 4,
    "medicine": "Rolck",
    "med_eat_time": "09:00 AM\n 03:50 PM\n 10:15 PM",
    "duration": 30,
    "due_duration": 10
  }
]

export const fetchMedicine = () => {
  return async (dispatch, getState) => {
   const key = KEYS.medicine
   const member_id = getMemberId(getState());
    try {
     dispatch(setLoading({ key, value: true }));
     const res = await fetch(`${GET_MEDICINE}member_id=${member_id}`).then(res => res.json());
     
     //----------------------------------------- TEST -----------------------------------
     
       // const res = await fetch(`https://my-json-server.typicode.com/j-rayhan/demo/medicines`).then(res => res.json());
      const result = med.map(v => {
        let o = Object.assign({}, v);
              o.med_eat_time = _.compact(v.med_eat_time.split(/\n/))
              return o;
      });
      console.log("DATA BASE DATA: ", result)
      let sortByTime = {};
      for (let index = 0; index < result.length; index++) {
        result[index].med_eat_time.reduce((r, time) => {
          const a = time.trim();
          sortByTime[a] = sortByTime[a] || [];
          sortByTime[a].push(result[index]);
          return sortByTime;
        }, Object.create(null));
      }
      dispatch(fetchSuccess({ key, value: result }));
      dispatch(fetchSuccess({ key: "alarm", value: sortByTime }));
    } catch (err) {
     console.error('PRINT IN %s=====>','Medicine', err);
    } finally {
     dispatch(setLoading({ key, value: false }));
    }
  }
 }


export const fetchFeedback = () => {
  return async (dispatch, getState) => {
   const key = KEYS.feedback
   const member_id = getMemberId(getState());
    try {
     dispatch(setLoading({ key, value: true }));
     //01010101 
     const res = await fetch(`${GET_FEEDBACK}member_id=${member_id}`).then(res => res.json());
     // console.log('PRINT IN %s=====>','Profile', res);
      dispatch(fetchSuccess({ key, value: res[0] }));
    } catch (err) {
     // console.error('PRINT IN %s=====>','Profile', err);
    } finally {
     dispatch(setLoading({ key, value: false }));
    }
  }
 }


export const sendFeedback = (message = '') => {
  return async (dispatch, getState) => {
   const key = KEYS.feedback
   const member_id = getMemberId(getState());
    try {
     dispatch(setLoading({ key, value: true }));
     //01010101 
     const res = await fetch(`${POST_FEEDBACK}member_id=${member_id}&message=${message}`).then(res => res.json());
     // const res = await fetch(`${GET_PROFILE}member_id=${userId}`).then(res => res.json());
     console.log('PRINT IN %s=====>','feed back res', res);
      return res;
    } catch (err) {
     // console.error('PRINT IN %s=====>','Profile', err);
    } finally {
     dispatch(setLoading({ key, value: false }));
    }
  }
 }

export const fetchPrescriptions = () => {
  return async (dispatch, getState) => {
   const key = KEYS.prescriptions
   const member_id = getMemberId(getState());
   //console.log('PRINT IN %s=====>','prescriptions', `${GET_PRESCRIPTION}member_id=${member_id}`);
    try {
     dispatch(setLoading({ key, value: true }));
     //01010101 

     const res = await fetch(`${GET_PRESCRIPTION}member_id=${member_id}`).then(res => res.json());
     // const res = await fetch(`${GET_PROFILE}member_id=${userId}`).then(res => res.json());
     //console.log('PRINT IN %s=====>','res', res);
      dispatch(fetchSuccess({ key, value: res }));
    } catch (err) {
     // console.error('PRINT IN %s=====>','Profile', err);
    } finally {
     dispatch(setLoading({ key, value: false }));
    }
  }
 }

export const fetchReoprts = () => {
  return async (dispatch, getState) => {
   const key = KEYS.reports
   const member_id = getMemberId(getState());
   console.log('PRINT IN %s=====>','reports', member_id);
    try {
     dispatch(setLoading({ key, value: true }));
     //01010101 
     const res = await fetch(`${GET_REPORT}member_id=${member_id}`).then(res => res.json());
     // const res = await fetch(`${GET_PROFILE}member_id=${userId}`).then(res => res.json());
     console.log('PRINT IN %s=====>','reports res', res);
      dispatch(fetchSuccess({ key, value: res }));
    } catch (err) {
     // console.error('PRINT IN %s=====>','Profile', err);
    } finally {
     dispatch(setLoading({ key, value: false }));
    }
  }
 }

export const uploadReoprt = (file) => {
  return async (dispatch, getState) => {
   const key = KEYS.reports
   const member_id = getMemberId(getState());
   console.log('PRINT IN %s=====>','reports', member_id);
    try {
     dispatch(setLoading({ key, value: true }));
     //01010101 
     const res = await fetch(`${POST_REPORT}member_id=${member_id}`,{
      method: 'post',
      body: file,
    }).then();
     // const res = await fetch(`${GET_PROFILE}member_id=${userId}`).then(res => res.json());
     console.log('PRINT IN %s=====>','reports res', res);
      dispatch(fetchReoprts());
    } catch (err) {
     // console.error('PRINT IN %s=====>','Profile', err);
    } finally {
     dispatch(setLoading({ key, value: false }));
    }
  }
 }

export const uploadPrescription = (file) => {
  return async (dispatch, getState) => {
   const key = KEYS.prescriptions
   const member_id = getMemberId(getState());
   console.log('PRINT IN %s=====>','reports', member_id);
    try {
     dispatch(setLoading({ key, value: true }));
     //01010101 
     const res = await fetch(`${POST_PRESCRIPTION}member_id=${member_id}`,{
      method: 'post',
      body: file,
    }).then();
     // const res = await fetch(`${GET_PROFILE}member_id=${userId}`).then(res => res.json());
     console.log('PRINT IN %s=====>','prescripttions res', res);
      dispatch(fetchPrescriptions());
    } catch (err) {
     // console.error('PRINT IN %s=====>','Profile', err);
    } finally {
     dispatch(setLoading({ key, value: false }));
    }
  }
 }