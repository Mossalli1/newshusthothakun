import { createAction } from 'redux-act';
//
import { KEYS } from '../../utils/constants';
import { setLoading } from './loading';
import { authService } from '../../App';
import { clearUser } from './user';
import { setGlobalSuccess } from './common';
//
export const LOGIN = createAction('loginSuccess');
export const LOGOUT = createAction('logoutSuccess');


export const handleLogin = payload => {
 return async dispatch => {
  const key = KEYS.login
   try {
    dispatch(setLoading({ key, value: true }));
    const res = await authService.login(payload).then(res => res.text()).then(data => data); 
    if(res !== 'true') alert(res)
    if(res === 'true'){
      dispatch(LOGIN({ key, value: res }));
      dispatch(setGlobalSuccess({key: KEYS.memberId, value: payload.memberId}))
      dispatch(setGlobalSuccess({key: KEYS.memberName, value: payload.name}))
    };
   } catch (err) {
    console.error('PRINT IN %s=====>','AuthActions', err);
   } finally {
    dispatch(setLoading({ key, value: false }));
   }
 }
}

export const handleLogout = () => {
  return async dispatch => {
   const key = KEYS.login
    try {
     dispatch(setLoading({ key, value: true }));
    //  console.log('PRINT IN %s=====>','Logout');
     dispatch(LOGIN({ key, value: false }));
     dispatch(clearUser());
     dispatch(setGlobalSuccess({key: KEYS.memberId, value: ''}))
     dispatch(setGlobalSuccess({key: KEYS.memberName, value: ''}))
    } catch (err) {
     console.error('PRINT IN %s=====>','AuthActions', err);
    } finally {
     dispatch(setLoading({ key, value: false }));
    }
  }
 }