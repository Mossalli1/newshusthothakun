import { createAction } from 'redux-act';
//
import { KEYS } from '../../utils/constants';
import { setLoading } from './loading';
import { GET_SERVICE_PACKAGE } from '../../utils/api';
//
export const fetchSuccess = createAction('fetchSuccess');
export const setGlobalSuccess = createAction('setGlobalSuccess');


export const fetchServicePackage = () => {
 return async dispatch => {
  const key = KEYS.services
   try {
    dispatch(setLoading({ key, value: true }));
    const res = await fetch(GET_SERVICE_PACKAGE).then(res => res.json());
    // console.log('PRINT IN %s=====>','AuthActions', res);
     dispatch(fetchSuccess({ key, value: res }));
   } catch (err) {
    console.error('PRINT IN %s=====>','Service', err);
   } finally {
    dispatch(setLoading({ key, value: false }));
   }
 }
}