import { createReducer } from 'redux-act';
import { fetchSuccess, setGlobalSuccess } from '../actions/common';

const initialState = {
  service: [],
}

const commonReducer = createReducer({
  [fetchSuccess]: (state, payload) => ({ ...state, [payload.key]: payload.value }),
  [setGlobalSuccess]: (state, payload) => ({ ...state, [payload.key]: payload.value })
}, initialState);

export default commonReducer;