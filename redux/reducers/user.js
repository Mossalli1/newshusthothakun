import { createReducer } from 'redux-act';
import { fetchSuccess, clearUser } from '../actions/user';

const initialState = {
  profile: {},
  medicine: [],
  reports: [],
  prescriptions: [],
  alarm: {},
  feedback: {},
}

const userReducer = createReducer({
  [fetchSuccess]: (state, payload) => ({ ...state, [payload.key]: payload.value }),
  [clearUser]: state => ({ ...state, ...initialState }),
}, initialState);

export default userReducer;