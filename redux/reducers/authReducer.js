import { createReducer } from 'redux-act';
import { LOGIN, LOGOUT } from '../actions/auth';

const initialState = {
  login: false,
}

const authReducer = createReducer({
  [LOGIN]: (state, payload) => ({ ...state, [payload.key]: payload.value })
}, initialState);

export default authReducer;