// import {Notifications} from 'expo';
import React from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
//
import { fetchMedicine } from '../redux/actions/user';
import { KEYS } from '../utils/constants';

import PushNotification from 'react-native-push-notification'
import PushNotificationIOS from "@react-native-community/push-notification-ios";



const timeArray = [new Date(Date.now() + 10 * 1000), new Date(Date.now() + 10 * 5000)]
class AlarmService{

  constructor(onRegister, onNotification) {
    this.configure(onRegister, onNotification);
    this.state = {
      notification: {}
    }

    this.lastId = 0;
  }

    configure = (onRegister, onNotification, gcm = "") => {
      PushNotification.configure({
        // (optional) Called when Token is generated (iOS and Android)
        onRegister: function(token) {
          console.log("TOKEN:", token);
        },
      
        // (required) Called when a remote or local notification is opened or received
        onNotification: function(notification) {
          // console.log("NOTIFICATION2:", notification);
          
          // process the notification
      
          // required on iOS only (see fetchCompletionHandler docs: https://github.com/react-native-community/react-native-push-notification-ios)
          notification.finish(PushNotificationIOS.FetchResult.NoData);
        },
      
        // ANDROID ONLY: GCM or FCM Sender ID (product_number) (optional - not required for local notifications, but is need to receive remote push notifications)
        senderID: "YOUR GCM (OR FCM) SENDER ID",
      
        // IOS ONLY (optional): default: all - Permissions to register.
        permissions: {
          alert: true,
          badge: true,
          sound: true
        },
      
        // Should the initial notification be popped automatically
        // default: true
        popInitialNotification: true,
      
        /**
         * (optional) default: true
         * - Specified if permissions (ios) and token (android and ios) will requested or not,
         * - if not, you must call PushNotificationsHandler.requestPermissions() later
         */
        requestPermissions: true,
      });
    }

    

      alarm = () =>{
        timeArray.map((t, i)=>{
          console.log('ttt',t);
          PushNotification.localNotificationSchedule({
            title: "My Notification Title", // (optional)
            message: "My Notification", // (required)
            date: t,
            soundName: 'alarm_clock_sound.caf',
        
            category: 'ff',
            userInfo: {id: '123', route: 'alarm', message: 'Time to Take Medicine', medicinName: ['napac', 'Rolac'] }
            // alertAction: <View><Text>tt</Text></View>
          });
    
        })
        
      }

      checkPermission= (cbk) => {
        return PushNotification.checkPermissions(cbk);
      }
    
      cancelNotif =() => {
        PushNotification.cancelLocalNotifications({id: ''+this.lastId});
      }
    
      cancelAll = () => {
        // PushNotification.cancelAllLocalNotifications();
        PushNotificationIOS.cancelAllLocalNotifications();
      }

}

const mapState = ({ loading, user }) => ({
    isLoading: !!loading[KEYS.profile] || !!loading[KEYS.reports] || !!loading[KEYS.prescriptions],
    medicine: user[KEYS.medicine],
  });
    
  const mapDispatch = dispatch => bindActionCreators({
    fetchMedicine
  }, dispatch);

// export default connect(mapState, mapDispatch) (AlarmService);
export default AlarmService;