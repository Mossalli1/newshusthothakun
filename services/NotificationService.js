// import {Notifications} from 'expo';
import * as Permissions from 'expo-permissions';
import { PUSH_REGISTRATION_ENDPOINT, MESSAGE_ENPOINT } from '../utils/api';

export class NotificationService {
    async registerForPushNotifications() {
      console.log('Call Function........')
        const { status: existingStatus } = await Permissions.getAsync(
            Permissions.NOTIFICATIONS
        );
        let finalStatus = existingStatus;
        if (existingStatus !== 'granted') {
            // Android remote notification permissions are granted during the app
            // install, so this will only ask on iOS
            const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            finalStatus = status;
        }
        console.log('File not Granted', finalStatus)
        if (finalStatus !== 'granted') {
            return;
        }

        let response;
        try {
            // let token = await Notifications.getExpoPushTokenAsync();
            response = await fetch(PUSH_REGISTRATION_ENDPOINT, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                  token: {
                    value: token,
                  },
                  user: {
                    username: 'test',
                    memberId: '01010101'
                  },
                }),
              });
            console.log('response------------------------>', response)

            return response;
        } catch (error) {
            console.error('ERROR***=============>', error);
            throw error;
        }
    }
/*
    sendMessage = async (message: string) => {
       const res = fetch(MESSAGE_ENPOINT, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            message
          }),
        });

        return res;
      }
*/
}