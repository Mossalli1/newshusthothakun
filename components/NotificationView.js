import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { Layout } from '../utils/appUtils'

const Demo = props => {
  return (
    <View style={{ height: Layout.window.height, backgroundColor: '#05C2CB', alignItems: 'center'}}>

                <View style={{
                  marginTop: 50, padding: 25, width: Layout.window.width
                }}>
                    <TouchableOpacity onPress={() => props.setNotification(false)} style={{height: 40, width: 120, backgroundColor: 'red', borderRadius: 5, alignItems: 'center', justifyContent: 'center'}}>
                    <Text>Close</Text>
                    </TouchableOpacity>
                
                </View>
                <View style={{alignItems: 'flex-start', justifyContent: 'center', padding: 20}}>
                  <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FFF' }}>
                    {props.message}
                  </Text>
                </View>
    </View>
  );
};

export default Demo;

const styles = StyleSheet.create({
  container: { flex: 1 }
});