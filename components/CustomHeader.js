import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text , SafeAreaView} from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import Colors from '../constants/Colors';
import DeviceInfo from 'react-native-device-info';

const deviceNotch = DeviceInfo.hasNotch()

const CustomHeader = props => {

  

  console.log('Info', deviceNotch)
  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => props.onPress()}
        style={{ width: 30, alignItems: 'center' }}
      >
        <FontAwesome name={props.name} size={22} color="#fff" />
      </TouchableOpacity>
      <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FFF' }}>
        {props.title}
      </Text>
      <TouchableOpacity onPress={() => props.onPressRight()}></TouchableOpacity>
    </View>
  );
};

export default CustomHeader;

const styles = StyleSheet.create({
  container: {
    height: deviceNotch ? 65 : 55,
    // paddingTop: 10,
    paddingBottom: 6,
    paddingLeft: 10,
    paddingRight: 15,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    backgroundColor: Colors.primaryColor
  }
});
