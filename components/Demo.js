import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

const Demo = props => {
  return (
    <View style={styles.container}>
      <Text style={{ fontSize: 18, fontWeight: 'bold' }}>SHUSTHO THAKUN</Text>
    </View>
  );
};

export default Demo;

const styles = StyleSheet.create({
  container: { flex: 1 }
});
