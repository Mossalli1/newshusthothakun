import React from 'react';
import {View, Text, ActivityIndicator, Modal} from 'react-native';
export function ProgressView({ visible }) {
  // console.log('PRINT IN %s=====>','ProgressView', visible);
    return (
      <Modal onRequestClose={() => null} visible={visible} transparent>
        <View
          style={{
            flex: 1,
            backgroundColor: "rgba(0,0,0,0.5)",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <View
            // style={{ borderRadius: 10, backgroundColor: "white", padding: 25 }}
          >
            {/*<Text style={{ fontSize: 20, fontWeight: "200" }}>Loading...</Text>*/}
            <ActivityIndicator color="#224eba" size="large"/>
          </View>
        </View>
      </Modal>
    );
  };