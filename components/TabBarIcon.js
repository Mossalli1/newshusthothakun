import React from 'react';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';

import Colors from '../constants/Colors';

export default function TabBarIcon(props) {
  return (
    <MaterialIcons
      name={props.name}
      size={20}
      style={{ marginBottom: -3 }}
      color="#000"
      // color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
    />
  );
}
