import { Dimensions } from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export const Layout = {
  window: {
    width,
    height
  },
  fontsize: 18,
  isSmallDevice: width < 375
};

export const getMemberId = state => state && state.common && state.common.memberId ;