
const ROOT_API = "http://shusthothakun.com/app/" 
export const ROOT_IMAGES_API = "http://shusthothakun.com/images/" 
//Members/
// Login
// login_controller.php?member_name=xxx&member_id=xxx
export const LOGIN = `${ROOT_API}login_controller.php?`;

// Service & Package
// service_package.php
export const GET_SERVICE_PACKAGE = `${ROOT_API}service_package.php?`;

// Notice 
// notice.php
export const GET_NOTICE = `${ROOT_API}notice.php?`;

// Notification
// notification_handler.php?member_id=xxx
export const GET_NOTIFICATION  = `${ROOT_API}notification_handler.php?`;

// Report upload
// report_upload.php?member_id=xxx
export const POST_REPORT = `${ROOT_API}report_upload.php?`;

// Prescription Upload
// UploadImage.php?member_id=xxx
export const POST_PRESCRIPTION = `${ROOT_API}UploadImage.php?`;

// Profile
// view.php?type=member&member_id=xxx
export const GET_PROFILE = `${ROOT_API}view.php?type=member&`;

// Medicine
// view.php?type=medicine&member_id=xxx
export const GET_MEDICINE = `${ROOT_API}view.php?type=medicine&`;

// Prescription
// view.php?type=prescription&member_id=xxx
export const GET_PRESCRIPTION = `${ROOT_API}view.php?type=prescription&`;

// Report
// view.php?type=report&member_id=xxx
export const GET_REPORT = `${ROOT_API}view.php?type=report&`;

// Doctor Consultant
// view.php?type=doctorconsultant&member_id=xxx
export const GET_DOCTOR = `${ROOT_API}view.php?type=doctorconsultant&`;

// Feedback upload
// complain_handle.php?member_id=xxx&message=xxx
export const POST_FEEDBACK = `${ROOT_API}complain_handle.php?`;

// Feedback View
// complain_handle.php?member_id=xxx
export const GET_FEEDBACK = `${ROOT_API}complain_handle.php?`;

// export const  = `${ROOT_API}`

export const SERVER_API = 'https://e1c930ba.ngrok.io';
export const PUSH_REGISTRATION_ENDPOINT = `${SERVER_API}/token`
export const MESSAGE_ENPOINT  = `${SERVER_API}/message`