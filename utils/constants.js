export const KEYS = {
  login: 'login',
  memberId: 'memberId',
  memberName: 'memberName',
  logout: 'logout',
  profile: 'profile',
  services: 'services',
  medicine: 'medicine',
  serviceDetails: 'serviceDetails',
  feedback: 'feedback',
  prescriptions: 'prescriptions',
  reports: 'reports',
 }