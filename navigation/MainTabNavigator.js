import React from 'react';
import { Platform, Linking } from 'react-native';
import {
  createStackNavigator,
  createDrawerNavigator,
  // createMaterialTopTabNavigator
} from 'react-navigation';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
// import { createDrawerNavigator } from 'react-navigation-drawer';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import Profile from '../screens/profile/Profile';
import Medicine from '../screens/profile/Medicine';
import Colors from '../constants/Colors';
import Prescriptions from '../screens/profile/Prescriptions';
import Reports from '../screens/profile/Reports';
import Doctors from '../screens/profile/Doctors';
import Layout from '../constants/Layout';
import { Ionicons, FontAwesome, Entypo, AntDesign } from '@expo/vector-icons';
import DrawerView from '../screens/drawer/DrawerView';
// import Home from '../screens/drawer/Home';
import Feedback from '../screens/drawer/Feedback';
// import ContactUs from '../screens/contacts/ContactUs';
import AboutUs from '../screens/drawer/AboutUs';
import Services from '../screens/services/Services';
import ServiceDetails from '../screens/services/ServiceDetails';
import Packages from '../screens/packages/Packages';
import ContactUs from '../screens/contacts/ContactUs';
// import Site from '../screens/drawer/Site';

// const config = Platform.select({
//   web: { headerMode: 'screen' },
//   default: {}
// });

//Profile tabs

const profileStack = createStackNavigator({
  Profile: Profile
});

profileStack.navigationOptions = {
  tabBarLabel: 'Profile',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name="person-outline" size={40} />
  )
};

const medicineStack = createStackNavigator({
  Medicine: Medicine
});

medicineStack.navigationOptions = {
  tabBarLabel: 'Medicine',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name="local-pharmacy" />
  )
};

const prescriptionStack = createStackNavigator({
  Prescritions: Prescriptions
});

prescriptionStack.navigationOptions = {
  tabBarLabel: 'Prescription',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name="assignment" />
  )
};

const reportStack = createStackNavigator({
  Reports: Reports
});

reportStack.navigationOptions = {
  tabBarLabel: 'Report',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name="speaker-notes" />
  )
};

const doctorStack = createStackNavigator({
  Doctors: Doctors
});

doctorStack.navigationOptions = {
  tabBarLabel: 'Doctor',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name="assignment-ind" />
  )
};

const profileTabs = createMaterialTopTabNavigator(
  {
    Profile: profileStack,
    Medicine: medicineStack,
    Prescription: prescriptionStack,
    Report: reportStack,
    Doctor: doctorStack
  },
  {
    tabBarOptions: {
      showIcon: true,
      tabStyle: {
        height: 50,
        width: Layout.window.width / 3.2
      },
      labelStyle: {
        fontSize: 12,
        marginTop: 0,
        color: '#000'
      },
      style: {
        backgroundColor: Colors.tabBackground
      },
      indicatorStyle: {
        backgroundColor: 'red'
      },
      scrollEnabled: true
    }
  }
);

profileTabs.navigationOptions = {
  // headerLeft: <FontAwesome name="bars" size={22} color="#fff" />,
  headerRight: null,
  title: 'Profile',
  headerStyle: {
    backgroundColor: Colors.tabBackground,
    borderBottomColor: Colors.tabBackground,
    height: Platform.OS=='ios'? 22 : 48
  },
  headerTitleStyle: {
    // backgroundColor: '#fff',
    color: '#fff'
  }
};

const stackNav = createStackNavigator({
  Home: HomeScreen,
  Profile: profileTabs,
  Services: {
    screen: Services,
    navigationOptions: {
      title: 'Services'
    }
  },
  ServiceDetails: {
    screen: ServiceDetails,
    navigationOptions: {
      title: 'Services Details'
    }
  },
  Packages: {
    screen: Packages,
    navigationOptions: {
      title: 'Packages'
    }
  },
  ContactUs: {
    screen: ContactUs,
    navigationOptions: {
      title: 'Contact us'
    }
  }
});

stackNav.navigationOptions = {
  // header: null
};

const MyDrawerNavigator = createDrawerNavigator(
  {
    HomeScreen: {
      screen: stackNav,
      navigationOptions: {
        drawerLabel: 'Home',
        drawerIcon: <FontAwesome name="home" size={24} />
      }
    },
    FeedbackScreen: {
      screen: Feedback,
      navigationOptions: {
        drawerLabel: 'Feedback',
        drawerIcon: <Ionicons name="ios-chatbubbles" size={24} />
      }
    },
    // ContactScreen: {
    //   screen: ContactUs,
    //   navigationOptions: {
    //     drawerLabel: 'Contact us',
    //     drawerIcon: <Entypo name="phone" size={24} />
    //   }
    // },
    AboutScreen: {
      screen: AboutUs,
      navigationOptions: {
        drawerLabel: 'About us',
        drawerIcon: <AntDesign name="infocirlce" size={24} />
      }
    }
    // SiteScreen: {
    //   screen: Site,
    //   navigationOptions: {
    //     drawerLabel: 'Site',
    //     drawerIcon: <FontAwesome name="internet-explorer" size={24} />
    //   }
    // }
  },
  {
    initialRouteName: 'HomeScreen',
    contentComponent: DrawerView,
    drawerWidth: Layout.window.width - 85,
    // hideStatusBar: true,
    contentOptions: {
      activeTintColor: '#242e42',
      itemsContainerStyle: {
        // marginVertical: 30
      }
    }
  }
);

MyDrawerNavigator.navigationOptions = {};

// const HomeStack = createStackNavigator(
//   {
//     Home: HomeScreen,
//   },
//   config
// );

// HomeStack.navigationOptions = {
//   tabBarLabel: 'Home',
//   tabBarIcon: ({ focused }) => (
//     <TabBarIcon
//       focused={focused}
//       name={
//         Platform.OS === 'ios'
//           ? `ios-information-circle${focused ? '' : '-outline'}`
//           : 'md-information-circle'
//       }
//     />
//   ),
// };

// HomeStack.path = '';

// const tabNavigator = createBottomTabNavigator({
//   HomeStack,
//   LinksStack,
//   SettingsStack,
// });

// tabNavigator.path = '';

export default MyDrawerNavigator;
