const tintColor = '#fff';

export default {
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
  tabBackground: '#4fc3f7',
  primaryColor: '#05C2CB',
  buttonColor: '#26c6da',
  textCoor: '#636363',
  webPrimary: '#6CC9F4'
};
