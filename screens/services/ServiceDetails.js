import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Image,
  ScrollView
} from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { connect } from 'react-redux';
//
import { KEYS } from '../../utils/constants';
import Colors from '../../constants/Colors';
// source={{uri: service[`${SERVICE}_image`]}}
// const test_iamge = require('../../assets/icon/medeat.png');
// const test_iamge = '../../assets/icon/medeat.png';
// const test_iamge2 = '../../assets/images/robot-dev.png';
// const test_iamge3 = '../../assets/images/robot-prod.png';
// const test_iamge4 = '../../assets/icon/Purchase.jpg';
const SERVICE = 'service';

const ServiceDetails = props => {
  const service = props.serviceDetails;
  return (
    <ScrollView style={styles.container}>
      <View
        key={service[`${SERVICE}_id`]}
        style={{
          borderRadius: 5,
          margin: 12,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 5
          },
          shadowOpacity: 0.3,
          shadowRadius: 3,
          elevation: 5,
          backgroundColor: '#fff'
        }}
      >
        <View
          style={{
            backgroundColor: Colors.primaryColor,
            borderTopRightRadius: 5,
            borderTopLeftRadius: 5,
            // height: 40,
            alignItems: 'center',
            justifyContent: 'center',
            padding: 12
          }}
        >
          <Text style={{ fontSize: 20, fontWeight: 'bold' }}>
            {service[`${SERVICE}_name`]}
          </Text>
        </View>

        <View style={{ padding: 12 }}>
          <Text>{service[`${SERVICE}_details`]}</Text>
        </View>
      </View>
    </ScrollView>
  );
};

const mapState = ({ common }) => ({
  serviceDetails: common[KEYS.serviceDetails] || {}
});

export default connect(mapState)(ServiceDetails);

const styles = StyleSheet.create({
  container: { flex: 1 }
});
