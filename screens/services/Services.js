import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Image,
  ScrollView
} from 'react-native';
import { FontAwesome, AntDesign } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
//
import { fetchServicePackage, setGlobalSuccess } from '../../redux/actions/common';
import { KEYS } from '../../utils/constants';
import Colors from '../../constants/Colors';
// source={{uri: service[`${SERVICE}_image`]}}
// const test_iamge = require('../../assets/icon/medeat.png');
const test_iamge = '../../assets/icon/medeat.png';
const test_iamge2 = '../../assets/images/robot-dev.png';
const test_iamge3 = '../../assets/images/robot-prod.png';
const test_iamge4 = '../../assets/icon/Purchase.jpg';

const SERVICE = 'service';
const Services = props => {
  console.log('PRINT IN %s=====>', 'Service', props);

  const readMore = (service) => {
      props.setGlobalSuccess({key: KEYS.serviceDetails, value: service});
      props.navigation.navigate('ServiceDetails');
  }
  // console.log('PRINT IN %s=====>','Services', `../../assets/icon/${props.services[0].service_image}`);
  // console.log('PRINT IN %s=====>','statick', test_iamge);
  return (
    <ScrollView style={styles.container}>
      {props.services.map((service, index) => {
        // const imgSrc = `../../assets/images/${service[`${SERVICE}_image`]}`
        // console.log('PRINT IN %s=====>','map image', imgSrc);
        // let imgS;
        // switch (service[`${SERVICE}_image`]) {
        //   case 'GoldenHour.jpg':
        //   console.log('PRINT IN %s=====>','golden hour',);
        //     imgS = test_iamge2
        //     break;

        //   default:
        //     break;
        // }
        // console.log('PRINT IN %s=====>','Services', imgS);
        return (
          <View
            key={index}
            style={{
              borderRadius: 5,
              margin: 12,
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 5
              },
              shadowOpacity: 0.3,
              shadowRadius: 3,
              elevation: 5,
              backgroundColor: '#fff'
            }}
          >
            <View
              style={{
                backgroundColor: Colors.primaryColor,
                borderTopRightRadius: 5,
                borderTopLeftRadius: 5,
                // height: 40,
                alignItems: 'center',
                justifyContent: 'center',
                padding: 12
              }}
            >
              <Text style={{ fontSize: 20, fontWeight: 'bold' }}>
                {service[`${SERVICE}_name`]}
              </Text>
            </View>

            <View style={{ padding: 12, paddingBottom: 3 }}>
              {/* <Text>{service[`${SERVICE}_image`]}</Text> */}
              {/* {imgS !== undefined ? (
              <Image
                source={require(test_iamge2)}
                style={{ width: 100, height: 100 }}
              />
            ) : (
              <Image
                source={require(test_iamge)}
                style={{ width: 100, height: 100 }}
              />
            )} */}

              <Text>{service[`${SERVICE}_details`].substr(0, 200)}...</Text>
              <TouchableOpacity
                style={{
                  alignItems: 'center',
                  width: '100%',
                  flexDirection: 'row',
                  height: 35
                }}
                activeOpacity={0.5}
              >
                <Text style={{ fontWeight: 'bold' }} onPress={() => readMore(service)}>Read More</Text>
                <AntDesign
                  name="caretright"
                  size={14}
                  color={Colors.primaryColor}
                />
              </TouchableOpacity>
            </View>
          </View>
        );
      })}
    </ScrollView>
  );
};

const mapState = ({ loading, common }) => ({
  isLoading: !!loading[KEYS.services],
  services: common[KEYS.services].service || []
});

const mapDispatch = dispatch =>
  bindActionCreators(
    {
      fetchServicePackage,
      setGlobalSuccess,
    },
    dispatch
  );

export default connect(mapState, mapDispatch)(Services);

const styles = StyleSheet.create({
  container: { flex: 1 }
});
