import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import CustomHeader from '../../components/CustomHeader';
import MapView from 'react-native-maps';
import Layout from '../../constants/Layout';

const ContactUs = props => {
  return (
    <View style={styles.container}>
      {/* <CustomHeader
        title="Contact us"
        onPress={() => props.navigation.openDrawer()}
        name="bars"
      /> */}
      <View style={{ flex: 1 }}>
        <MapView style={{ flex: 1 }}>
          <MapView.Marker
            coordinate={{
              latitude: 23.78927,
              longitude: 90.421006
            }}
            title="Shusthothakun"
          />
        </MapView>
      </View>

      <View
        style={{ padding: 5, width: Layout.window.width, alignItems: 'center' }}
      >
        <Text style={{ color: '#585757', fontWeight: 'bold', fontSize: 17 }}>
          Shusthothakun
        </Text>
        <Text style={{ color: '#585757', fontWeight: 'bold' }}>
          #H 06 (Lake Side), #R 104, Gulshan-2, Dhaka-1212
        </Text>
        <Text style={{ color: '#585757', fontWeight: 'bold' }}>
          Call: +8809612114500
        </Text>
        <Text style={{ color: '#585757', fontWeight: 'bold' }}>
          Email: <Text style={{ color: 'red' }}>info@shusthothakun.com</Text>
        </Text>
      </View>
    </View>
  );
};

// ContactUs.navigationOptions = {
//   header: null
// };

export default ContactUs;

const styles = StyleSheet.create({
  container: { flex: 1 }
});
