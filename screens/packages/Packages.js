import React, { useEffect } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  ScrollView
  // SafeAreaView
} from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
//
import { fetchServicePackage } from '../../redux/actions/common';
import { KEYS } from '../../utils/constants';
import Colors from '../../constants/Colors';

const PACKAGE = 'package';
const Packages = props => {
  console.log('PRINT IN %s=====>', 'Packages', props);

  return (
    <ScrollView style={styles.container}>
      {props.packages.map((value, index) => {
        return (
          <View
            key={index}
            style={{
              borderRadius: 5,
              margin: 12,
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 5
              },
              shadowOpacity: 0.3,
              shadowRadius: 3,
              elevation: 5,
              backgroundColor: '#fff'
            }}
          >
            <View
              style={{
                backgroundColor: Colors.primaryColor,
                borderTopRightRadius: 5,
                borderTopLeftRadius: 5,
                // height: 40,
                alignItems: 'center',
                justifyContent: 'center',
                padding: 12
              }}
            >
              <Text style={{ fontSize: 20, fontWeight: 'bold' }}>
                {value[`${PACKAGE}_name`]}
              </Text>
              <Text>{value[`${PACKAGE}_moto`]}</Text>
            </View>

            <View style={{ padding: 12 }}>
              <Text>{value[`${PACKAGE}_service`]}</Text>
              <Text
                style={{ color: 'green', fontSize: 18, fontWeight: 'bold' }}
              >
                Price: {value[`${PACKAGE}_price`]} BDT
              </Text>
            </View>
          </View>
        );
      })}
    </ScrollView>
  );
};

const mapState = ({ loading, common }) => ({
  isLoading: !!loading[KEYS.services],
  packages: common[KEYS.services].package || []
});

const mapDispatch = dispatch =>
  bindActionCreators(
    {
      fetchServicePackage
    },
    dispatch
  );

export default connect(mapState, mapDispatch)(Packages);

const styles = StyleSheet.create({
  container: { flex: 1 }
});
