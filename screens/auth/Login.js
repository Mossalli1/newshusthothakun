import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  TextInput,
  Image,
  Platform,
  KeyboardAvoidingView
} from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
//
import { fetchServicePackage } from '../../redux/actions/common';
import { handleLogin } from '../../redux/actions/auth';
import { KEYS } from '../../utils/constants';
import Colors from '../../constants/Colors';
import { ProgressView } from '../../components/Loading';

const Login = props => {
  const [name, setName] = useState(undefined);
  const [errorMessage, setErrorMessage] = useState(undefined);
  const [memberId, setMemberId] = useState(undefined);
  const [behavior, setBehavior] = useState('position')
  useEffect(() => {
    props.fetchServicePackage();
  }, []);
  const onSubmit = () => {
    props.handleLogin({ name, memberId });
  };
  useEffect(() => {
    if (props.isLogin === 'true') {
      props.navigation.navigate('Main');
    }
  }, [props.isLogin]);
  return (
    <View style={styles.container}>
      <View style={{ top: 120, alignItems: 'center', width: '100%' }}>
        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#DFDFDF' }}>
          LOG IN
        </Text>
        <Text style={{ fontSize: 16, color: '#DFDFDF' }}>
          Login and view your all activity
        </Text>

        <View style={{ height: 150, marginTop: 20 }}>
          <Image
            source={require('../../assets/icon/Sustho-Thakun-Logo-3.jpg')}
            style={{ height: 120, width: 120, borderRadius: 60 }}
          />
        </View>
        
        {Platform.OS=='ios'? <View
            style={styles.inputContainerIos}
          >
            <TextInput
              placeholder="Username"
              value={name}
              onChangeText={tx => setName(tx)}
              style={{ height: 20, width: '100%', paddingLeft: 15 }}
            />
            <View
              style={styles.inputDeviderIos}
            />
            <TextInput
              placeholder="Member id"
              value={memberId}
              onChangeText={tx => setMemberId(tx)}
              style={{height: 20, width: '100%', paddingLeft: 15 }}
            />
          </View>
          :
          <View style={styles.inputContainer}
          >
            <TextInput
                placeholder="Username"
                value={name}
                onChangeText={tx => setName(tx)}
                style={{ width: '100%', paddingLeft: 15}}
            />
            <View
                style={styles.inputDevider}
            />
            
            <TextInput
                placeholder="Member id"
                value={memberId}
                onChangeText={tx => setMemberId(tx)}
                style={{width: '100%', paddingLeft: 15 }}
            />
          </View>
          
        }
        
        <TouchableOpacity
          activeOpacity={0.6}
          onPress={() => onSubmit()}
          style={styles.loginButton}
        >
          <Text style={{ color: 'red', fontSize: 18, fontWeight: 'bold' }}>
            LOG IN
          </Text>
        </TouchableOpacity>

        <View
          style={{
            width: '85%',
            marginTop: 15
          }}
        >
          <Text style={{ color: '#D15F8F', fontSize: 12, fontWeight: 'bold' }}>
            DON'T HAVE AN ACCOUNT? VISIT SHUSTHO THAKUN OFFICE.
          </Text>
        </View>
        <ProgressView visible={props.isLoading} />
      </View>
    </View>

  );
};

const mapState = ({ loading, auth }) => ({
  isLoading: !!loading[KEYS.login],
  isLogin: auth[KEYS.login]
});

const mapDispatch = dispatch =>
  bindActionCreators(
    {
      fetchServicePackage,
      handleLogin
    },
    dispatch
  );

export default connect(mapState, mapDispatch)(Login);

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#05C2CB', },
  inputContainerIos: {
    backgroundColor: '#DFDFDF',
    borderRadius: 5,
    width: '90%',
    paddingTop: 15,
    paddingBottom: 15,
    marginTop: 20
  },
  inputDeviderIos: {
    width: '100%',
    height: 1,
    backgroundColor: '#989797',
    marginTop: 15,
    marginBottom: 15
  },
  inputContainer: {
    backgroundColor: '#DFDFDF',
    borderRadius: 5,
    width: '90%',
    marginTop: 20
  },
  inputDevider: {
    width: '100%',
    height: 1,
    backgroundColor: '#989797',
  },
  loginButton: {
    width: '90%',
    backgroundColor: '#DFDFDF',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    height: 45
  }
});
