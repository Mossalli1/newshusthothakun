import React, { useState } from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Linking,
  SafeAreaView
} from 'react-native';

import { MonoText } from '../components/StyledText';
import Layout from '../constants/Layout';
import CustomHeader from '../components/CustomHeader';
import CallTo from './home/components/CallTo';
import TopScrollButton from './home/components/TopScrollButtons';
import Colors from '../constants/Colors';
import Timer from './profile/components/LocalNotificationsNew';

const HomeScreen = props => {
  const [openDialog, setOpenDialog] = useState(false);
  return (
    <View style={styles.container}>
      <CustomHeader
        onPress={() => props.navigation.openDrawer()}
        name="bars"
        title="SHUSTHO THAKUN"
      />

      <TopScrollButton onPress={name => props.navigation.navigate(name)} />

      <View style={styles.iconContainer}>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => setOpenDialog(!openDialog)}
          style={{ position: 'absolute' }}
        >
          <Image
            source={require('../assets/icon/icon.png')}
            style={{ height: 130, width: 130, marginBottom: 70 }}
          />
        </TouchableOpacity>
        {openDialog && (
          <CallTo
            callManager={() => Linking.openURL(`tel:${'+8809612114500'}`)}
            callDoctor={() => Linking.openURL(`tel:${'+8809612114501'}`)}
          />
        )}
      </View>

      <View
        style={{
          backgroundColor: '#EBEAEA',
          marginTop: -150,
          marginLeft: 10,
          marginRight: 10,
          borderRadius: 5,
          padding: 5
        }}
      >
        <Text
          style={{ fontSize: 17, fontWeight: 'bold', color: Colors.textCoor }}
        >
          Tips for a Healthy Heart
        </Text>
        <Text style={{ color: Colors.textCoor, marginTop: 2, fontSize: 14 }}>
          Put your hands to work to help your mind unwind. Engaging in
          activities such as knitting, sewing, and crocheting can help relieve
          stress and do your ticker some good. Other relaxing hobbies, such as
          woodworking, cooking, or completing jigsaw puzzles, may also help take
          the edge off stressful days.
        </Text>
      </View>
      <Timer />
    </View>
  );
};

HomeScreen.navigationOptions = {
  header: null
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    height: Layout.window.height,
    backgroundColor: '#fff',
    flex: 1
  },
  iconContainer: {
    height: Layout.window.height - 125,
    alignItems: 'center',
    justifyContent: 'center'
    // backgroundColor: 'red'
    // marginBottom: 60
  }
});
