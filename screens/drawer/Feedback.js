import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  TextInput,
  ScrollView,
  Linking
} from 'react-native';
import { FontAwesome, Ionicons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
//
import CustomHeader from '../../components/CustomHeader';
import Colors from '../../constants/Colors';
import { fetchFeedback, sendFeedback } from '../../redux/actions/user';
import { KEYS } from '../../utils/constants';

const Feedback = props => {
  const [message, setMessage] = useState(undefined);
  const [errorMessage, setErrorMessage] = useState(undefined);
  useEffect(() => {
    props.fetchFeedback();
  }, []);
  const onSubmit = async () => {
    const res = await props.sendFeedback(message);
    if (res) {
      alert('Success');
      setMessage(undefined);
      props.fetchFeedback();
    } else {
      alert('Something Worng!');
    }
  };

  const { feedback } = props;
  return (
    <View style={styles.container}>
      <View>
        <CustomHeader
          title="Feedback"
          onPress={() => props.navigation.openDrawer()}
          name="bars"
        />

        <View
          style={{
            backgroundColor: '#fff',
            width: '100%',
            marginTop: 10,
            marginLeft: 10,
            marginRight: 10,
            borderRadius: 7,
            shadowColor: '#000'
          }}
        >
          <ScrollView
            style={{
              width: '95%'
            }}
            contentContainerStyle={{ alignItems: 'center' }}
          >
            <Text style={{ fontWeight: 'bold' }}>Feedback</Text>
            <View
              style={{ width: 200, height: 1, backgroundColor: '#858585' }}
            />
            <View
              style={{
                width: '100%',
                paddingTop: 5,
                paddingBottom: 5
                // backgroundColor: 'red'
              }}
            >
              <Text>Date: {feedback.date}</Text>
              <Text>{feedback.complain}</Text>
            </View>

            <Text style={{ fontWeight: 'bold' }}>Response</Text>
            <View
              style={{ width: 200, height: 1, backgroundColor: '#858585' }}
            />
            <View style={{ width: '100%', paddingTop: 5, paddingBottom: 5 }}>
              <Text>Date: {feedback.solution_date}</Text>
              <Text>{feedback.solution}</Text>
            </View>
          </ScrollView>
        </View>
        <View
          style={{
            backgroundColor: '#fff',
            width: '95%',
            marginTop: 10,
            marginLeft: 10,
            marginRight: 10,
            borderRadius: 3,
            borderColor: Colors.primaryColor,
            borderWidth: 1,
            height: 200,
            padding: 5
          }}
        >
          <TextInput
            onChangeText={tx => setMessage(tx)}
            value={message}
            placeholder="Message"
          />
        </View>

        <View
          style={{
            width: '100%',
            alignItems: 'flex-end',
            justifyContent: 'center',
            paddingRight: 15,
            marginTop: 20
          }}
        >
          <View
            style={{
              backgroundColor: Colors.buttonColor,
              height: 35,
              paddingLeft: 20,
              paddingRight: 20,
              borderRadius: 2,
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <Text onPress={() => onSubmit()}>SEND</Text>
          </View>
        </View>
      </View>

      <View
        style={{
          width: '100%',
          alignItems: 'flex-end',
          justifyContent: 'center',
          paddingRight: 15,
          marginTop: 20,
          marginBottom: 10
        }}
      >
        <TouchableOpacity
          style={{
            backgroundColor: '#f44336',
            width: 50,
            height: 50,
            borderRadius: 50 / 2,
            alignItems: 'center',
            justifyContent: 'center'
          }}
          onPress={() => Linking.openURL(`tel:${'+8809612114500'}`)}
        >
          <Ionicons name="md-call" size={22} color="#fff" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

Feedback.navigationOptions = {
  header: null
};

const mapState = ({ loading, user }) => ({
  isLoading: !!loading[KEYS.feedback],
  feedback: user[KEYS.feedback]
});

const mapDispatch = dispatch =>
  bindActionCreators(
    {
      fetchFeedback,
      sendFeedback
    },
    dispatch
  );

export default connect(mapState, mapDispatch)(Feedback);

const styles = StyleSheet.create({
  container: { flex: 1, justifyContent: 'space-between' }
});
