import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Linking
} from 'react-native';
import { FontAwesome, Entypo } from '@expo/vector-icons';
import { DrawerItems } from 'react-navigation-drawer';
import { connect } from 'react-redux';
import { handleLogout } from '../../redux/actions/auth';

const DrawerView = props => {
  const logout = () => {
    props.dispatch(handleLogout());
    props.navigation.navigate('Login');
  }
  return (
    <View style={styles.container}>
      <View>
        <DrawerItems {...props} />
        <TouchableOpacity
          style={{
            // borderTopWidth: 1,
            // borderTopColor: '#CDCDCD',
            height: 55,
            paddingLeft: 17,
            flexDirection: 'row',
            alignItems: 'center'
          }}
          onPress={() => Linking.openURL('http://shusthothakun.com/')}
        >
          <FontAwesome name="internet-explorer" size={24} color="#616161" />
          <Text style={{ marginLeft: 32, fontWeight: 'bold' }}>Site</Text>
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => logout()}
        style={{
          borderTopWidth: 1,
          borderTopColor: '#CDCDCD',
          height: 55,
          paddingLeft: 20,
          flexDirection: 'row',
          alignItems: 'center'
        }}
      >
        <Entypo name="log-out" size={26} />
        <Text style={{ marginLeft: 20, fontWeight: 'bold' }}>Logout</Text>
      </TouchableOpacity>
    </View>
  );
};

export default connect(null)(DrawerView);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 28,
    justifyContent: 'space-between'
  }
});
