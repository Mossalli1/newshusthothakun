import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import CustomHeader from '../../components/CustomHeader';
import Colors from '../../constants/Colors';
import { ScrollView } from 'react-native-gesture-handler';

const AboutUs = props => {
  return (
    <View style={styles.container}>
      <CustomHeader
        onPress={() => props.navigation.openDrawer()}
        name="bars"
        title="About us"
      />
      <ScrollView>
        <View style={{ padding: 5, paddingLeft: 10, paddingRight: 10 }}>
          <Text
            style={{ fontSize: 17, fontWeight: 'bold', color: Colors.textCoor }}
          >
            WHO WE ARE
          </Text>

          <Text style={{ color: Colors.textCoor, marginTop: 2, fontSize: 14 }}>
            Shusthothakun offers a unique combination of premier health care and
            community-based social services to Banani, Gulshan, Baridhara,
            Bashundhara, area in Dhaka city. Researches have indicated that
            there is a significant need for quality health care and social
            services within this region and we believe that by employing
            competent and well-educated staff and providing them with organized
            and responsive management, we can become the health care/social
            service provider for the senior citizens of those areas.
          </Text>

          <Text style={{ color: Colors.textCoor, marginTop: 10, fontSize: 14 }}>
            Shusthothakun Health Services has been created as a project under
            Arpan Limited. The initial office has been established in quality
            office space at Gulshan 2 as it will be the center point of our
            service providing area. Arpan Ltd and Particularly the Founder
            Directors and the Development Advisor provided the personal
            impetuses for the services and developed by the expert team. We
            believe that the commitment of our Medical team will ensure
            professional dedication to the highest degree.
          </Text>

          <Text style={{ color: Colors.textCoor, marginTop: 10, fontSize: 14 }}>
            Members of our services will be those individuals and families in
            need of health care and social services. Those members will be given
            full health supports such as (a) Medical Profile, (b) Golden Hour,
            (c) Regular Medical Check-Up, (d) Doctor's Consultation, (e)
            Medicine Reminder, (f) Hospital Admission Financial Backup etc. Our
            organization has already developed an excellent reputation with many
            health professionals, hospitals and e-med service providers. Our
            organization has all the licenses and clearances needed for
            providing community health services by government of Bangladesh.
          </Text>

          <Text style={{ color: Colors.textCoor, marginTop: 10, fontSize: 14 }}>
            Few establishments are providing some of our services in this region
            but not the whole package that we are giving to senior citizens of
            our service area. They also do not offer the unique blend of health
            care and social services which Shusthothakun will be providing.
          </Text>

          <Text
            style={{
              fontSize: 17,
              fontWeight: 'bold',
              color: Colors.textCoor,
              marginTop: 15
            }}
          >
            MISSION
          </Text>

          <Text style={{ color: Colors.textCoor, marginTop: 10, fontSize: 14 }}>
            Our values are simple. Shusthothakun wellness Services strives to
            offer excellent and affordable health care and community-based
            social services to individuals and families. It is our goal to
            employ competent, caring, and well-trained individuals who are
            responsive to the needs of our members, their families and the
            communities we serve. In turn, our institution will provide staff
            with competitive compensation, an inviting work environment, and
            knowledgeable, trustworthy management and direction.
          </Text>

          <Text
            style={{
              fontSize: 17,
              fontWeight: 'bold',
              color: Colors.textCoor,
              marginTop: 15
            }}
          >
            OUR COMMITMENTS TO OUR MEMBERS
          </Text>

          <View style={{ marginTop: 10, alignItems: 'flex-start' }}>
            <Text style={styles.textStyle}>
              # Full medical profile management for members for local and
              overseas treatment.
            </Text>
            <Text style={styles.textStyle}>
              # Ensuring the Professional quality of services offered.
            </Text>
            <Text style={styles.textStyle}>
              # Reliability : being available through on-call, and adequate
              staffing
            </Text>
            <Text style={styles.textStyle}>
              # Effective collaboration with other community professionals
              (physicians, hospitals, and other organizations)
            </Text>
          </View>

          <Text
            style={{
              fontSize: 17,
              fontWeight: 'bold',
              color: Colors.textCoor,
              marginTop: 15
            }}
          >
            OUR COMMITMENTS TO OUR MEMBERS
          </Text>

          <View style={{ marginTop: 10, alignItems: 'flex-start' }}>
            <Text style={[styles.textStyle, { marginBottom: 5 }]}>
              Shusthothakun Health Services is a new Health and Social Service
              organization in Bangladesh. Our organization will provide the
              following services to members from Banani, Baridhara, Bashundhara
              and Gulshan - all within a 15 kilometers radius of our office:
            </Text>
            <Text style={styles.textStyle}># GOLDEN HOURS</Text>
            <Text style={styles.textStyle}># REGULAR MEDICAL CHECK-UP</Text>
            <Text style={styles.textStyle}># DOCTORS' PHONE CONSULTATION</Text>
            <Text style={styles.textStyle}># DIGITAL MEDICAL PROFILE</Text>
            <Text style={styles.textStyle}># MEDICINE REMINDER</Text>
            <Text style={styles.textStyle}>
              #HOSPITAL ADMISSION CASH BACKUP IN IMMINENT DANGER
            </Text>
            <Text style={styles.textStyle}># PURCHASING OF MEDICINES</Text>
            <Text style={styles.textStyle}># DIAGNOSTIC CENTER</Text>
            <Text style={styles.textStyle}># AMBULANCE SERVICE</Text>
            <Text style={styles.textStyle}># DOCTOR'S APPOINTMENT</Text>
            <Text style={styles.textStyle}># DOCTORS' PHONE CONSULTATION</Text>
            <Text style={styles.textStyle}>
              # HOME VISIT BY NURSE (MALE/FEMALE) / PHYSIOTHERAPIST ( Coming
              soon )
            </Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default AboutUs;

const styles = StyleSheet.create({
  container: { flex: 1 },
  textStyle: { color: Colors.textCoor, fontSize: 14 }
});
