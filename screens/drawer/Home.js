import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

const Home = props => {
  return (
    <View style={styles.container}>
      <Text style={{ fontSize: 18, fontWeight: 'bold' }}>SHUSTHO THAKUN</Text>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: { flex: 1 }
});
