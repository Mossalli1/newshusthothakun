import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  ScrollView,
  Image,
} from 'react-native';
import { Avatar } from 'react-native-elements';
import { FontAwesome, Ionicons } from '@expo/vector-icons';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as FileSystem from 'expo-file-system';

import * as Permissions from 'expo-permissions';
//
import { fetchReoprts, uploadReoprt } from '../../redux/actions/user';
import { KEYS } from '../../utils/constants';
import { ROOT_IMAGES_API, POST_REPORT } from '../../utils/api';
import { Layout } from '../../utils/appUtils';
import Upload from './components/Upload';

const Reports = props => {
  const [togleAddimage, setToggleAddimage] = useState(false);
  const [image, setImage] = useState(null);
  const [images, setImages] = useState([]);
  const { isLoading, reports } = props;
    console.log('PRINT IN %s=====>','....',reports);
  useEffect(() => {
    props.fetchReoprts();
    getPermissionAsync();
  }, []);

 const getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(
        Permissions.CAMERA_ROLL,
        Permissions.CAMERA
      );
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  };

  const _pickImage = async () => {
    let imageList = images;
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      quality: 0.3,
      aspect: [4, 3]
    });

    // console.log("Hello " + result.object);

    if (!result.cancelled) {
      let localUri = result.uri;
      const fileInfo = await FileSystem.getInfoAsync(localUri, { md5: true, size: true });
      if (fileInfo.size > 500000) {
        alert("Sorry, your file is too large.")
      } else {

        imageList.push(result.uri);
        setImages(images.filter((val, id, array) => array.indexOf(val) == id));


        let fielName = localUri.split('/').pop();
        // Infer the type of the image
        let match = /\.(\w+)$/.exec(fielName);
        let type = match ? `image/${match[1]}` : `image`;

        // Upload the image using the fetch and FormData APIs
        let formData = new FormData();
        // Assume "photo" is the name of the form field the server expects
        formData.append('fileToUpload', { uri: localUri, name: fielName, type });
        props.uploadReoprt(formData);
      }
    }
  };

  const _pickImageByCamera = async () => {
    let imageList = images;
    let result = await ImagePicker.launchCameraAsync({
      allowsEditing: true,
      quality: 0.3,
      aspect: [4, 3]
    });

    
    if (!result.cancelled) {
      let localUri = result.uri;
      const fileInfo = await FileSystem.getInfoAsync(localUri, { md5: true, size: true });
      if (fileInfo.size > 500000) {
        alert("Sorry, your file is too large.")
      } else {

        imageList.push(result.uri);
        setImages(images.filter((val, id, array) => array.indexOf(val) == id));


        let fielName = localUri.split('/').pop();
        // Infer the type of the image
        let match = /\.(\w+)$/.exec(fielName);
        let type = match ? `image/${match[1]}` : `image`;

        // Upload the image using the fetch and FormData APIs
        let formData = new FormData();
        // Assume "photo" is the name of the form field the server expects
        formData.append('fileToUpload', { uri: localUri, name: fielName, type });
        props.uploadReoprt(formData);
      }
     
    }
  };

  return (
    <View style={styles.container}>
      <View style={{ padding: 15, paddingTop: 5 }}>
        {/* <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#000' }}>
          You haven't upload any report yet.
        </Text> */}

        <ScrollView>
          <View
            style={{
              width: Layout.window.width - 30,
              flexDirection: 'row',
              flexWrap: 'wrap',
              alignItems: 'flex-start',
              justifyContent: 'space-around'
            }}
          >
            {reports && reports.map((image, index) => {
              return (
                <View
                  key={index}
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 10
                    // marginLeft: 10,
                    // marginRight: 10,
                  }}
                >
                  <Avatar
                    source={{ uri: `${ROOT_IMAGES_API}reports/${image.image}` }}
                    size={150}
                    // style={{ borderRadius: 5 }}
                    // onPress={() => this.setState({ image: image })}
                  />
                  {/* <View
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      top: -40,
                      right: -102,
                      position: 'relative',
                      height: 30,
                      width: 30,
                      borderRadius: 30 / 2,
                      backgroundColor: '#fff'
                    }}
                  >
                    <Ionicons name="md-trash" size={22} color="#fb1010" />
                  </View> */}
                </View>
              );
            })}
          </View>
        </ScrollView>
      </View>
      <View style={{ alignItems: 'flex-end' }}>
        {togleAddimage && (
          <Upload
            upLoad={() => _pickImage()}
            upLoadByCamera={() => _pickImageByCamera()}
          />
        )}
        <TouchableOpacity
          onPress={() => setToggleAddimage(!togleAddimage)}
          style={{
            backgroundColor: 'red',
            height: 60,
            width: 60,
            borderRadius: 30,
            alignItems: 'center',
            justifyContent: 'center',
            marginRight: 15
          }}
        >
          <Ionicons name="md-image" size={24} color="#fff" />
        </TouchableOpacity>
      </View>
    </View>
  );
};
// <ViewReports img={re.image} date={re.date} img_path="reports"/>

 export const ViewReports = ({img, date, img_path }) => {
  console.log('PRINT IN %s=====>','uri',`${ROOT_IMAGES_API}${img_path}/${img}`);
  return (
    <View
          style={styles.headerImage}
        >
          <View style={styles.profileImageView}>
            <Image
              source={{
                uri: `${ROOT_IMAGES_API}${img_path}/${img}`
              }}
              style={styles.profileImage}
            />
          </View>
          
          <View style={styles.contentView}>
              <Text style={styles.nameText}>{date}</Text>
          </View>
    </View>
  )
}
Reports.navigationOptions = {
  header: null
};

const mapState = ({ loading, user }) => ({
  isLoading: !!loading[KEYS.reports],
  reports: user[KEYS.reports] || [],
});

const mapDispatch = dispatch => bindActionCreators({
  fetchReoprts,
  uploadReoprt,
}, dispatch);

export default connect(mapState,mapDispatch)(Reports);

const styles = StyleSheet.create({
  container: { flex: 1 },

  headerImage: {
    width: Layout.window.width,
    paddingLeft: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 20
  },
  profileImageView: {
    height: 120,
    width: 120,
    borderWidth: 2,
    top: 60,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderColor: '#fff',
    shadowColor: '#000',
    shadowOpacity: 0.3,
    shadowRadius: 5,
    shadowOffset: {
      width: 0,
      height: 5
    },
    elevation: 5,
    borderWidth: 2
  },

  profileImage: {
    height: 120,
    width: 120,
  },

  contentView: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 68
  },
  nameText: {
    fontSize: 22,
    color: '#000',
    fontWeight: 'bold',
    marginBottom: 5
  },
  container: { flex: 1, justifyContent: 'space-between', paddingBottom: 20 }
});
