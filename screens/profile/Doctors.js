import React, { useState, useEffect } from 'react';
import { StyleSheet, TouchableOpacity, View, Text, Platform } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import Layout from '../../constants/Layout';

//test
import * as Calendar from 'expo-calendar';
import * as Permissions from 'expo-permissions';
import moment from 'moment';

const eventDetails1 = {
  title: 'Time to take medicine',
  startDate: moment()
    .add(6, 'm')
    .toDate(),
  endDate: moment()
    .add(10, 'm')
    .toDate(),

  alarms: [{ relativeOffset: -5 }]
  // entityType: Calendar.EntityTypes.EVENT,
  // sourceId:
  //   Platform.OS === "ios"
  //     ? calendarEvent.find(
  //         cal => cal.source && cal.source.name === "Default"
  //       ).source.id
  //     : undefined,
  // name: "Apple",
  // accessLevel: Calendar.CalendarAccessLevel.OWNER
  //   timeZone: Localization.timezone
  // alarms (Array<Alarm>)

  /**** This causes the crash ****/
  //    alarms: [
  //       {
  //         method: Calendar.AlarmMethod.DEFAULT
  //       }
  //     ]
};
async function crte() {

  const { status } = await Permissions.askAsync(Permissions.CALENDAR);
  if (status === 'granted') {
    const calendars = await Calendar.getCalendarsAsync();
    console.log('mmmmmmm', { calendars });
  }
  const calendarEvent = await Calendar.getCalendarsAsync(
    Calendar.EntityTypes.EVENT
  );
  console.log('calendarEvent', JSON.stringify(calendarEvent));
  const eventDetails = {
    title: 'Test Event',
    startDate: moment()
      .add(1, 'm')
      .toDate(),
    endDate: moment()
      .add(5, 'm')
      .toDate(),
    entityType: Calendar.EntityTypes.EVENT,
    sourceId:
      Platform.OS === 'ios'
        ? calendarEvent.find(
            cal => cal.source && cal.source.name === 'Default'
          ).source.id
        : undefined,
    name: 'Apple',
    accessLevel: Calendar.CalendarAccessLevel.OWNER
    //   timeZone: Localization.timezone
    // alarms (Array<Alarm>)

    /**** This causes the crash ****/
    //    alarms: [
    //       {
    //         method: Calendar.AlarmMethod.DEFAULT
    //       }
    //     ]
  };
  

  // const sourceId =
  //   Platform.OS === "ios"
  //     ? calendarEvent.find(cal => cal.source && cal.source.name === "Default")
  //         .source.id
  //     : undefined;

  // console.log("id>>>>>>>>", sourceId);

  // this.setState({ calendarId: sourceId })

  const newCalender = await Calendar.createCalendarAsync(eventDetails);
  this.setState({ calendarId: newCalender });

  console.log('success calendar', JSON.stringify(newCalender));

  const createmyEvent = await Calendar.createEventAsync(
    this.state.calendarId,
    eventDetails1
  );

  console.log('id>>>>>>>>,,,,,,,,,,,,,,,,,,,,,,,', createmyEvent);
}

class Doctors extends React.Component{
    state = {
      ccId: ""
    };


    //Adding Calendar Event
    // componentDidMount = async () => {

    //   const { status } = await Permissions.askAsync(Permissions.CALENDAR);
    //   if (status === "granted") {
    //     const calendars = await Calendar.getCalendarsAsync();
    //     console.log("mmmmmmm", { calendars });
    //   }
      
    //   const calendarEvent = await Calendar.getCalendarsAsync(
    //     Calendar.EntityTypes.EVENT
    //   );
    //   console.log("calendarEvent", JSON.stringify(calendarEvent));
    //   const eventDetails = {
    //     title: "Test Event",
    //     startDate: moment()
    //       .add(1, "m")
    //       .toDate(),
    //     endDate: moment()
    //       .add(5, "m")
    //       .toDate(),
    //     entityType: Calendar.EntityTypes.EVENT,
    //     sourceId:
    //       Platform.OS === "ios"
    //         ? calendarEvent.find(
    //             cal => cal.source && cal.source.name === "iCloud"
    //           ).source.id
    //         : undefined,
    //     name: "Apple",
    //     accessLevel: Calendar.CalendarAccessLevel.OWNER
    //     //   timeZone: Localization.timezone
    //     // alarms (Array<Alarm>)
  
    //     /**** This causes the crash ****/
    //     //    alarms: [
    //     //       {
    //     //         method: Calendar.AlarmMethod.DEFAULT
    //     //       }
    //     //     ]
    //   };
      
  
    //   // const sourceId =
    //   //   Platform.OS === "ios"
    //   //     ? calendarEvent.find(cal => cal.source && cal.source.name === "Default")
    //   //         .source.id
    //   //     : undefined;
  
    //   // console.log("id>>>>>>>>", sourceId);
  
    //   // this.setState({ calendarId: sourceId })
  
    //   const newCalender = await Calendar.createCalendarAsync(eventDetails);
    //   this.setState({ calendarId: newCalender });
  
    //   console.log("success calendar", JSON.stringify(newCalender));
  
    //   const createmyEvent = await Calendar.createEventAsync(
    //     this.state.calendarId,
    //     eventDetails1
    //   );
  
    //   console.log("id>>>>>>>>", createmyEvent);
    // };

    
  
  render (){
    return (
      <View style={styles.container}>
        <View style={styles.doctorHeader}>
          <Text>Date</Text>
          <Text>Doctor name</Text>
          <Text>Disease</Text>
          <Text>Via</Text>
        </View>
      </View>
    );
  }
}

Doctors.navigationOptions = {
  header: null
};

export default Doctors;

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#eeeeee' },
  doctorHeader: {
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
    height: 35,
    width: Layout.window.width,
    borderBottomColor: '#9e9e9e',
    borderBottomWidth: 0.5,
    backgroundColor: '#fff'
  }
});
