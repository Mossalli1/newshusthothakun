import React, { useEffect } from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
//
import { fetchMedicine } from '../../redux/actions/user';
import { KEYS } from '../../utils/constants';
import MedicineTabHeader from './components/MedicineTabHeader';
import MedicineList from './components/MedicineList';
import AlarmClock from './components/AlarmSetTime';
import { ProgressView } from '../../components/Loading';
import Timer from './components/LocalNotificationsNew';
const Medicine = props => {
  useEffect(()=>{
    props.fetchMedicine();
  },[]);
  const { isLoading, medicine } = props;
  console.log('Data :', medicine)
  if(isLoading){
    return <ProgressView visible={props.isLoading} />
  }
  return (
    <View style={styles.container}>
      <MedicineTabHeader />

      {medicine && medicine.map((value, index) => {
        return (
          <MedicineList
            key={index}
            name={value.medicine}
            time={value.med_eat_time}
            // time={value.time[1]}
            duration={value.duration}
            dueDuration={value.due_duration}
          />
        );
      })}
      {/* <Timer /> */}
      {/* <AlarmClock /> */}
    </View>
  );
};

Medicine.navigationOptions = {
  header: null
};

const mapState = ({ loading, user }) => ({
  isLoading: !!loading[KEYS.profile] || !!loading[KEYS.reports] || !!loading[KEYS.prescriptions],
  medicine: user[KEYS.medicine],
});

const mapDispatch = dispatch => bindActionCreators({
  fetchMedicine
}, dispatch);

export default connect(mapState,mapDispatch)(Medicine);

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#F0F0F0' }
});
