import React, {Component} from 'react';
import {TextInput, View, Keyboard} from 'react-native';
// import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import * as Constants from 'expo-constants';
import { connect } from 'react-redux';
import moment from 'moment';

class Timer extends Component {
    onSubmit(e, alarmTime) {
        Keyboard.dismiss();
        const times = Object.keys(alarmTime);
        // console.log('PRINT IN %s=====>','submit', times);
        const localNotification = {
            title: 'track medichin',
            body: alarmTime[times[1]][0].medicine,
        };

        const schedulingOptions = {
            time: (new Date()).getTime() + Number(e.nativeEvent.text)
        }
// console.log('PRINT IN %s=====>','moment date', moment(moment().format('YYYY-MM-DD')).add(5, 'hours').add( 12, 'minutes').format('YYYY-MM-DD HH:mm'));
// console.log('PRINT IN %s=====>','LocalNotifications Time', (new Date()).getTime() + Number(e.nativeEvent.text));
        // Notifications show only when app is not active.
        // (ie. another app being used or device's screen is locked)
        // Notifications.scheduleLocalNotificationAsync(
        //     localNotification, schedulingOptions
        // );
    }

    handleNotification() {
        console.warn('ok! got your notif');
    }

    async componentDidMount() {
        // We need to ask for Notification permissions for ios devices
        let result = await Permissions.askAsync(Permissions.NOTIFICATIONS);

        if (Constants.isDevice && result.status === 'granted') {
            console.log('Notification permissions granted.')
        }

        // If we want to do something with the notification when the app
        // is active, we need to listen to notification events and 
        // handle them in a callback
        // Notifications.addListener(this.handleNotification);
    }

    render() {
        // console.log('PRINT IN %s=====>','LocalNotifications', this.props);
        const { alarmTime } = this.props;
        return (
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                <TextInput
                    onSubmitEditing={e => this.onSubmit(e, alarmTime)}
                    placeholder={'time in ms'}
                />
            </View>
        );
    }
};

const mapState = ({ user }) => ({ alarmTime: user.alarm || {}});

export default connect(mapState)(Timer);
