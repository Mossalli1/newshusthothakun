import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import Colors from '../../../constants/Colors';

const Upload = props => {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.button} onPress={() => props.upLoad()}>
        <Text>Choose Image</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.button, { marginLeft: 25 }]}
        onPress={() => props.upLoadByCamera()}
      >
        <Text>Take Photo</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Upload;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginRight: 20,
    marginBottom: 50
  },
  button: {
    backgroundColor: Colors.primaryColor,
    width: 120,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 3
  }
});
