import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import { Layout } from '../../../utils/appUtils';

class BodyProfile extends React.Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.itemContainer}>
          {/*<View style={styles.iconView}>
            <Image
              source={this.props.iconImage}
              style={this.props.imageStyle}
            />
          </View>*/}
          <View style={{ marginLeft: 30, flexDirection: 'column', width: "30%" }}>
            <Text style={{ color: '#6b6b6b', fontSize: 14 }}>
              {this.props.title}
            </Text>
            </View>
            <View>
              <Text style={styles.valueText}>: {this.props.value}</Text>
            </View>
        </View>

        {/*<View style={styles.horozontalLine} />*/}
      </View>
    );
  }
}

BodyProfile.navigationOptions = {
  header: null
};

export default BodyProfile;

const styles = StyleSheet.create({
  container: {
    padding: 0.5
    // backgroundColor: "red"
  },
  horozontalLine: {
    height: 2,
    width: Layout.window.width - 40,
    backgroundColor: 'rgba(168, 168, 168, 0.27)',
    marginTop: 20
  },
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 2,
    marginBottom: 3
  },
  iconView: {
    height: 35,
    width: 35,
    alignItems: 'center',
    justifyContent: 'center'
  },
  valueText: { color: '#272727', fontSize: 15 }
});