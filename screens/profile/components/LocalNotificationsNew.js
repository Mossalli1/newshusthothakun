import React, {useState, useEffect, Component} from 'react';
import {TextInput, View, Keyboard, Text, Navigator, Platform} from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import moment from 'moment';
import AlarmScreen from './AlarmScreen';
import Login from '../../auth/Login';
import { fetchMedicine } from '../../../redux/actions/user';
import { bindActionCreators } from "redux";
import PushNotification from 'react-native-push-notification'
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import AlarmService from '../../../services/AlarmService'

var alarmTimes = null;
var medicinName = [];
var _navigator;
const timeArray = [ 
  new Date(Date.now() + 10 * 1000), 
  new Date(Date.now() + 20 * 2000), 
  new Date(Date.now() + 20 * 3000),
  new Date(Date.now() + 20 * 4000),
  new Date(Date.now() + 20 * 5000),
  new Date(Date.now() + 20 * 6000),
  new Date(Date.now() + 20 * 7000),
  new Date(Date.now() + 20 * 8000),
  new Date(Date.now() + 20 * 10000),
  new Date(Date.now() + 20 * 11000),
  new Date(Date.now() + 20 * 12000),
  new Date(Date.now() + 20 * 13000),
  new Date(Date.now() + 20 * 14000),
  new Date(Date.now() + 20 * 15000),
  new Date(Date.now() + 20 * 16000),
  new Date(Date.now() + 20 * 17000),
  new Date(Date.now() + 20 * 18000),
  new Date(Date.now() + 20 * 19000),
  new Date(Date.now() + 20 * 20000),

];



class Timer extends React.Component{
  // state={
  //   notification: null
  // };
  constructor(props) {
    super(props);
    this.state = {
      notification: {},
      alarmTime : null,
    };
    this.lastId = 0;
    
    this.notif = new AlarmService();
  }

 async componentDidMount(){
    await this.props.fetchMedicine();
    Platform.OS=='ios' && await PushNotificationIOS.cancelAllLocalNotifications();
    // const noti= PushNotificationIOS.getInitialNotification();
    // console.log('Push>>>', noti)
    // this.props.fetchMedicine();
    console.log('Fired>>>>>1')
    await this._convertTime();
    await this._handleAlarm();
    Platform.OS=='ios' && await PushNotificationIOS.removeAllDeliveredNotifications();
    console.log('Fired>>>>>2')
    
  }
  
  _convertTime = () =>{
    const { alarmTime } = this.props;
    const times = Object.keys(alarmTime);

    // alarmTimes = times.length > 0 && times.map((time, i)=>{
    //   return moment(time, 'HH:mm A').toDate();
    // })
    
    const medicineSchedules = times.length > 0 && times.map((time, i)=>{
      medicinName = alarmTime[time].map( m => m.medicine);
      console.log('Medicine : ', time)
      return moment(time, 'HH:mm A').toDate();

    })
    // console.log('ala>>>>>', medicineSchedules)
    alarmTimes = medicineSchedules.length > 0 && medicineSchedules.filter(time=>time>=new Date(Date.now()));
    console.log('Filterded Time>>>>>', alarmTimes)
    // console.log('Filterded medicine>>>>>', medicinName)
  }


  _handleAlarm = () => {
    const { alarmTime } = this.props;
    this.lastId++
    alarmTimes.length > 0 && alarmTimes.map((t, i)=>{
      console.log('ttt',)
      const time = moment(t).format('hh:mm A');
      console.log('mmm>>>>>>',time);
      const medicinName = alarmTime[time].map( m => m.medicine);
      console.log('mmm',medicinName);
      PushNotification.localNotificationSchedule({
        // title: "My Notification Title", // (optional)
        message: "Time to take medicine", // (required)
        date: t,
        soundName: 'alarm_clock_sound.mp3',
        category: 'Medicine',
        userInfo: {id: ''+this.lastId, route: 'alarm', message: 'Time to Take Medicine', medicinName: medicinName },
        repeatType: 'day'
      });

    })

    Platform.OS=='ios' && PushNotificationIOS.removeAllDeliveredNotifications();

  }

    // renderScene(route, navigator){ 
    //   _navigator = navigator
    //   return <route.component navigator={navigator} />
    // }
  
    
  render(){
    
    console.log('the>>>>', this.lastId)
    console.log('Times are : ', this.state.alarmTime)

    console.log("State",this.state.notification)

    console.log('Medicine List :', medicinName)
    return(
      null
      
    )
  }

}
const mapState = ({ user }) => ({ alarmTime: user.alarm || {}});
const mapDispatch = dispatch => bindActionCreators({
  fetchMedicine
}, dispatch);

export default connect(mapState, mapDispatch)(Timer);
