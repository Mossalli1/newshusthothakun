import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

const headerTitles = [
  { name: 'Medicine' },
  { name: 'Eat time' },
  { name: 'Duration' },
  { name: 'Due duration' }
];

const MedicineTabHeader = props => {
  return (
    <View style={styles.container}>
      {headerTitles.map((items, key) => {
        return (
          <Text key={key} style={{ fontSize: 15, fontWeight: 'bold' }}>
            {items.name}
          </Text>
        );
      })}
    </View>
  );
};

MedicineTabHeader.navigationOptions = {
  header: null
};

export default MedicineTabHeader;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 35,
    paddingLeft: 15,
    paddingRight: 15
  }
});
