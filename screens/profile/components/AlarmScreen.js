import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity, View, Text, Image } from 'react-native';
import Layout from '../../../constants/Layout';

const medicineData = [
  {
    id: 1,
    medicine: 'NAPA',
    time: ['09:00 AM', '01:00 PM', '10:00 PM'],
    duration: 30,
    dueDuration: 7
  },
  {
    id: 2,
    medicine: 'Anziloc',
    time: ['09:05 AM', '01:05 PM', '10:05 PM'],
    duration: 10,
    dueDuration: 5
  },
  {
    id: 3,
    medicine: 'Bumetanide (Bumex)',
    time: ['09:00 AM', '01:10 PM', '10:10 PM'],
    duration: 20,
    dueDuration: 9
  },
  {
    id: 4,
    medicine: 'Rolak',
    time: ['09:15 AM', '03:50 PM', '10:15 PM'],
    duration: 30,
    dueDuration: 10
  }
];

const AlarmScreen = props => {
  return (
    <View style={styles.container}>
      <View
        style={{
          marginTop: 10,
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center'
        }}
      >
        <Image
          source={require('../../../assets/icon/shusthothakun.jpg')}
          style={{ height: 70, width: '85%' }}
        />
      </View>

      <View
        style={{
          marginTop: 15,
          flex: 3,
          alignItems: 'center',
          justifyContent: 'center'
        }}
      >
        <Image
          source={require('../../../assets/icon/medeat.png')}
          style={{ height: 190, width: '45%' }}
        />

        <Text style={{ marginTop: 20 }}>Medicine name</Text>
        {props.medicinName && props.medicinName.map(name => 
          <Text key={name} style={{ fontSize: 18, fontWeight: 'bold' }}>{name}</Text>
        )}
      </View>
      <View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingLeft: 20,
            paddingRight: 5,
            marginBottom: 2
          }}
        >
          <Text>Did you took the medicine?</Text>
          <View
            style={{
              flexDirection: 'row',
              width: 115,
              justifyContent: 'space-between'
            }}
          >
            <TouchableOpacity
              style={styles.yesNoButton}
              onPress={() => props.yesButton()}
            >
              <Text>YES</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.yesNoButton}
              onPress={() => props.laterButton()}
            >
              <Text>LATER</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <TouchableOpacity
        style={{
          width: Layout.window.width,
          height: 50,
          backgroundColor: '#FF0000',
          alignItems: 'center',
          justifyContent: 'center'
        }}
        onPress={() => props.stopButton()}
      >
        <Text style={{ color: '#fff' }}>STOP ALARM</Text>
      </TouchableOpacity>
    </View>
  );
};

AlarmScreen.navigationOptions = {
  header: null
};

export default AlarmScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    height: Layout.window.height,
    justifyContent: 'space-between'
    // position: 'absolute',
    // top: 0
  },
  yesNoButton: {
    backgroundColor: '#C4C4C4',
    width: 55,
    height: 35,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
