import React, { useState, useEffect } from 'react';
import { SafeAreaView, ScrollView, StyleSheet, Text, View, Alert } from 'react-native';
import Constants from 'expo-constants';
import { connect } from 'react-redux';
import _ from 'lodash';

function formatAMPM() {
 const date = new Date();
 var hours = date.getHours();
 var minutes = date.getMinutes();
 var ampm = hours >= 12 ? 'PM' : 'AM';
 hours = hours % 12;
 hours = hours ? (hours < 10 ? '0'+hours : hours) : 12; // the hour '0' should be '12'
 minutes = minutes < 10 ? '0'+minutes : minutes;
 var strTime = hours + ':' + minutes + ' ' + ampm;
 return strTime;
}
//---------------------------------------------- not work after closing app
class AlarmClock extends React.Component {
  constructor() {
    super();
    this.state = {
      currentTime: '',
      alarmTime: []
    };
  }

  componentDidMount(){
    this.clock = setInterval(
      () => this.setCurrentTime(),
      5000
    )
    this.interval = setInterval(
      () => this.checkAlarmClock(),
    5000);
    this.getValue();
  }

  componentWillUnmount(){
    // clearInterval(this.clock);
    // clearInterval(this.interval);
  }

  getValue = async () => {
   console.log('PRINT IN %s=====>','AlarmSetTime', this.props);
  const { alarmTime } = this.props;
  const times = Object.keys(alarmTime);
  this.setState({alarmTime: times})
}
  setCurrentTime(){
   console.log('PRINT IN %s=====>','set time');
    const date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? (hours < 10 ? '0'+hours : hours) : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;

    this.setState({
      currentTime: strTime
    });
  }

  checkAlarmClock(){
    const { alarmTime, currentTime } = this.state;
    if(alarmTime.length > 0) {
      console.log('alaram..')
      alarmTime.map(time => time == currentTime ? alert('its time') : '');
    } else {
      console.log('Not yet..')
    }   
  }


  render() {
    console.log('PRINT IN %s=====>','Duration', this.state);
    return (
      <Text>Show Alaram</Text>
    )
  }
}


const styles = StyleSheet.create({
 container: {
   flex: 1,
   marginTop: Constants.statusBarHeight,
 },
 scrollView: {
   backgroundColor: 'pink',
   marginHorizontal: 20,
 },
 text: {
   fontSize: 42,
 },
});


const mapState = ({ user }) => ({ alarmTime: user.alarm || {}});

export default connect(mapState)(AlarmClock);