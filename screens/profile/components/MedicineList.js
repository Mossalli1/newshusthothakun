import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import Layout from '../../../constants/Layout';

const MedicineList = props => {
  return (
    <View style={styles.container}>
      <View
        style={{
          width: (Layout.window.width / 4) * 1,
          alignItems: 'center',
          // backgroundColor: 'blue',
          paddingLeft: 5
        }}
      >
        <Text style={{ textAlign: 'center' }}>{props.name}</Text>
      </View>
      <View
        style={{
          width: (Layout.window.width / 4) * 1,
          alignItems: 'flex-start'
          // backgroundColor: 'green'
        }}
      >
        <Text>{props.time}</Text>
      </View>
      <View
        style={{
          width: (Layout.window.width / 4) * 0.65,
          alignItems: 'flex-start'
          // backgroundColor: 'blue'
        }}
      >
        <Text>{props.duration}</Text>
      </View>
      <View
        style={{
          width: (Layout.window.width / 4) * 1,
          alignItems: 'center',
          // backgroundColor: 'green',
          paddingRight: 10,
          marginRight: 12
          //   marginLeft: -15
        }}
      >
        <Text>{props.dueDuration}</Text>
      </View>
    </View>
  );
};

MedicineList.navigationOptions = {
  header: null
};

export default MedicineList;

const styles = StyleSheet.create({
  container: {
    // backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // height: 35,
    borderBottomColor: '#D5D5D5',
    borderBottomWidth: 1,
    paddingBottom: 10,
    paddingTop: 10
    // paddingLeft: 5,
    // paddingRight: 5
  }
});
