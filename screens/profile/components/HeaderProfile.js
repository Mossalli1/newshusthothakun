import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  ImageBackground,
  ScrollView,
  Text
} from 'react-native';
import { Layout } from '../../../utils/appUtils';
import { ROOT_IMAGES_API } from '../../../utils/api';
// /home/johir/all-test/native-react/shusthothakun/assets/images/icon.png
const HeaderProfile = props => {
  // console.log('PRINT IN %s=====>','images',`${ROOT_IMAGES_API}Members/${props.img}`);
  const { name, img, id, userPackage } = props;
  const upper = name && name.charAt(0).toUpperCase() + name.substring(1);
    return (
      <View style={styles.container}>
        <View
          style={styles.headerImage}
        >
          <View style={styles.profileImageView}>
            <Image
              source={{
                uri: `${ROOT_IMAGES_API}Members/${img}`
              }}
              style={styles.profileImage}
            />
          </View>
          
          <View style={styles.contentView}>
              <Text style={styles.nameText}>{upper}</Text>
            <View style={styles.buttonContainer}>
              <Text style={{ fontSize: 14, color: '#fff' }}>{id}</Text>
            </View>
            <View style={styles.buttonContainer}>
              <Text style={{ fontSize: 14, color: '#fff' }}>{userPackage}</Text>
            </View>
          </View>
          </View>
      </View>
    );
  }

HeaderProfile.navigationOptions = {
  header: null
};

export default HeaderProfile;

const styles = StyleSheet.create({
  container: {
    // flex: 1
  },
  headerImage: {
    width: Layout.window.width,
    backgroundColor: '#89cbe9',
    height: 260,
    paddingLeft: 10,
    paddingRight: 10,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 20
  },
  profileImageView: {
    height: 120,
    width: 120,
    borderRadius: 99999,
    borderWidth: 2,
    top: 60,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderColor: '#fff',
    shadowColor: '#000',
    shadowOpacity: 0.3,
    shadowRadius: 5,
    shadowOffset: {
      width: 0,
      height: 5
    },
    elevation: 5,
    borderWidth: 2
  },

  profileImage: {
    height: 120,
    width: 120,
    borderRadius: 999999
  },

  contentView: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 68
  },
  nameText: {
    fontSize: 22,
    color: '#fff',
    fontWeight: 'bold',
    marginBottom: 5
  },
  buttonContainer: {
    borderColor: '#4072ec',
    alignItems: 'center',
    justifyContent: 'center',
    // height: 30,
    width: 105
  },
  horozontalLine: {
    height: 2,
    width: Layout.window.width,
    backgroundColor: 'rgba(168, 168, 168, 0.27)',
    marginTop: 20
  }
});