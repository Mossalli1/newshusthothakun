import React, { useEffect } from 'react';
import { StyleSheet, TouchableOpacity, View, Text, ScrollView } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
//
import { GET_PROFILE } from '../../utils/api';
import { fetchProfile } from '../../redux/actions/user';
import { KEYS } from '../../utils/constants';
import { ProgressView }from '../../components/Loading'
import { Layout } from '../../utils/appUtils';
import HeaderProfile from './components/HeaderProfile';
import BodyProfile from './components/BodyProfile';

const Profile = props => {
  useEffect(() => {
    props.fetchProfile();
  },[]);
  console.log('PRINT IN %s=====>','PROFILE', props);
  const { isLoading, profile, memberName, memberId } = props;
  const { address, blood_group, contact_p, date_of_birth, email, emg_address, emg_contact_1, emg_name, emg_relationship, family_history, father_name, gender, height, id, image, marital_status, mother_name, weight } = profile || {}
  /*if(isLoading){
    return <ProgressView visible={props.isLoading} />
  }*/
  return (
    <View style={styles.container}>
        <HeaderProfile name={memberName} img={image} id={memberId} userPackage={profile.package} />
        <ScrollView>
          <BodyProfile title={"Father's Name  "} value={father_name} />
          <BodyProfile title={"Mother's Name  "} value={mother_name} />
          <BodyProfile title={"Email  "} value={email} />
          <BodyProfile title={"Gender  "} value={gender} />
          <BodyProfile title={"Blood group  "} value={blood_group} />
          <BodyProfile title={"Date of birth  "} value={date_of_birth} />
          <BodyProfile title={"Contact  "} value={contact_p} />
          <BodyProfile title={"Address  "} value={address} />
          <View>
              <Text style={{color: "#999", marginLeft: 10, marginTop: 20}}>Emergency Contact</Text>
          </View>
          <View style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
              <View style={styles.horozontalLine} />
          </View>
          <BodyProfile title={"Name  "} value={emg_name} />
          <BodyProfile title={"Relationship  "} value={emg_relationship} />
          <BodyProfile title={"Contact  "} value={emg_contact_1} />
          <BodyProfile title={"Address  "} value={emg_address} />
          <View>
              <Text style={{color: "#999", marginLeft: 10, marginTop: 20}}>Family History</Text>
          </View>
          <View style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
              <View style={styles.horozontalLine} />
          </View>
          <BodyProfile title={"Marital status"} value={marital_status} />
          <BodyProfile title={"Height  "} value={height} />
          <BodyProfile title={"Weight  "} value={weight} />
          <BodyProfile title={"History  "} value={family_history} />
        </ScrollView>
    </View>
  );                                                                                                                                                                                                                                                                                        
};

Profile.navigationOptions = {
  header: null
};

const mapState = ({ loading, user, common }) => ({
  isLoading: !!loading[KEYS.profile],
  profile: user[KEYS.profile],
  memberName: common[KEYS.memberName],
  memberId: common[KEYS.memberId],
});

const mapDispatch = dispatch =>
  bindActionCreators(
    {
      fetchProfile
    },
    dispatch
  );

export default connect(mapState, mapDispatch)(Profile);

const styles = StyleSheet.create({
  container: { flex: 1 },
  horozontalLine: {
    height: 2,
    width: Layout.window.width - 20,
    backgroundColor: 'rgba(168, 168, 168, 0.27)',
  },
});
