import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  ScrollView,
  Image
} from 'react-native';
import { FontAwesome, Ionicons } from '@expo/vector-icons';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as FileSystem from 'expo-file-system';
//
import { fetchPrescriptions, uploadPrescription } from '../../redux/actions/user';
import { KEYS } from '../../utils/constants';

import { Avatar } from 'react-native-elements';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import Upload from './components/Upload';
import Layout from '../../constants/Layout';
import { ROOT_IMAGES_API } from '../../utils/api';

const Prescriptions = props => {
  const [togleAlarm, setToggleAlarm] = useState(false);
  const [togleAddimage, setToggleAddimage] = useState(false);
  const [image, setImage] = useState(null);
  const [images, setImages] = useState([]);
const { prescriptions } = props;
  useEffect(() => {
    props.fetchPrescriptions();
    getPermissionAsync();
  }, []);
  console.log('PRINT IN %s=====>','....',props.prescriptions);

 const getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(
        Permissions.CAMERA_ROLL,
        Permissions.CAMERA
      );
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  };

  const _pickImage = async () => {
    let imageList = images;
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      quality: 0.3,
      aspect: [4, 3]
    });

    // console.log("Hello " + result.object);

    if (!result.cancelled) {
      let localUri = result.uri;
      const fileInfo = await FileSystem.getInfoAsync(localUri, { md5: true, size: true });
      if (fileInfo.size > 500000) {
        alert("Sorry, your file is too large.")
      } else {

        imageList.push(result.uri);
        setImages(images.filter((val, id, array) => array.indexOf(val) == id));


        let fielName = localUri.split('/').pop();
        // Infer the type of the image
        let match = /\.(\w+)$/.exec(fielName);
        let type = match ? `image/${match[1]}` : `image`;

        // Upload the image using the fetch and FormData APIs
        let formData = new FormData();
        // Assume "photo" is the name of the form field the server expects
        formData.append('fileToUpload', { uri: localUri, name: fielName, type });
        props.uploadPrescription(formData);
      }
    }
  };

  const _pickImageByCamera = async () => {
    let imageList = images;
    let result = await ImagePicker.launchCameraAsync({
      allowsEditing: true,
      quality: 0.3,
      aspect: [4, 3]
    });

    // console.log("Hello " + result.object);

    if (!result.cancelled) {
      let localUri = result.uri;
      const fileInfo = await FileSystem.getInfoAsync(localUri, { md5: true, size: true });
      if (fileInfo.size > 500000) {
        alert("Sorry, your file is too large.")
      } else {

        imageList.push(result.uri);
        setImages(images.filter((val, id, array) => array.indexOf(val) == id));


        let fielName = localUri.split('/').pop();
        // Infer the type of the image
        let match = /\.(\w+)$/.exec(fielName);
        let type = match ? `image/${match[1]}` : `image`;

        // Upload the image using the fetch and FormData APIs
        let formData = new FormData();
        // Assume "photo" is the name of the form field the server expects
        formData.append('fileToUpload', { uri: localUri, name: fielName, type });
        props.uploadPrescription(formData);
      }
    }
  };

  return (
    <View style={styles.container}>
      <View style={{ padding: 15, paddingTop: 5 }}>
        {/* <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#000' }}>
          You haven't upload any report yet.
        </Text> */}

        <ScrollView>
          <View
            style={{
              width: Layout.window.width - 30,
              flexDirection: 'row',
              flexWrap: 'wrap',
              alignItems: 'flex-start',
              justifyContent: 'space-around'
            }}
          >
            {prescriptions && prescriptions.map((image, index) => {
              return (
                <View
                  key={index}
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 10
                    // marginLeft: 10,
                    // marginRight: 10,
                  }}
                >
                  <Avatar
                    source={{ uri: `${ROOT_IMAGES_API}prescriptions/${image.image}` }}
                    size={150}
                    // style={{ borderRadius: 5 }}
                    // onPress={() => this.setState({ image: image })}
                  />
                  {/* <View
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      top: -40,
                      right: -102,
                      position: 'relative',
                      height: 30,
                      width: 30,
                      borderRadius: 30 / 2,
                      backgroundColor: '#fff'
                    }}
                  >
                    <Ionicons name="md-trash" size={22} color="#fb1010" />
                  </View> */}
                </View>
              );
            })}
          </View>
        </ScrollView>
      </View>
      <View style={{ alignItems: 'flex-end' }}>
        {togleAddimage && (
          <Upload
            upLoad={() => _pickImage()}
            upLoadByCamera={() => _pickImageByCamera()}
          />
        )}
        <TouchableOpacity
          onPress={() => setToggleAddimage(!togleAddimage)}
          style={{
            backgroundColor: 'red',
            height: 60,
            width: 60,
            borderRadius: 30,
            alignItems: 'center',
            justifyContent: 'center',
            marginRight: 15
          }}
        >
          <Ionicons name="md-image" size={24} color="#fff" />
        </TouchableOpacity>
      </View>
    </View>

    // <View style={styles.container}>
    //   <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#000' }}>
    //     SHUSTHO THAKUN
    //   </Text>

    //   <TouchableOpacity onPress={() => setToggleAlarm(!togleAlarm)}>
    //     <Text>Open Alarm Screen</Text>
    //   </TouchableOpacity>
    //   {togleAlarm && (
    //     <AlarmScreen
    //       YesButton={() => setToggleAlarm(!togleAlarm)}
    //       LaterButton={() => setToggleAlarm(!togleAlarm)}
    //       StopButton={() => setToggleAlarm(!togleAlarm)}
    //     />
    //   )}
    // </View>
  );
};

Prescriptions.navigationOptions = {
  header: null
};

const mapState = ({ loading, user }) => ({
  isLoading: !!loading[KEYS.prescriptions],
  prescriptions: user[KEYS.prescriptions] || [],
});

const mapDispatch = dispatch => bindActionCreators({
  fetchPrescriptions,
  uploadPrescription
}, dispatch);

export default connect(mapState,mapDispatch)(Prescriptions);

const styles = StyleSheet.create({
  container: { flex: 1, justifyContent: 'space-between', paddingBottom: 20 }
});
