import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  ScrollView
} from 'react-native';

const buttonItems = [
  {
    name: 'Services',
    navName: 'Services',
    color: '#00bcd4'
  },
  {
    name: 'Packages',
    navName: 'Packages',
    color: '#4db6ac'
  },
  {
    name: 'Profile',
    navName: 'Profile',
    color: '#29b6f6'
  },
  // {
  //   name: 'Feedback',
  //   navName: 'Feedback',
  //   color: '#4db6ac'
  // },
  {
    name: 'Contact us',
    navName: 'ContactUs',
    color: '#78909c'
  },
  // {
  //   name: 'About us',
  //   navName: 'AboutUs',
  //   color: '#00bcd4'
  // },
  // {
  //   name: 'Site',
  //   navName: 'Site',
  //   color: '#78909c'
  // }
];

const TopScrollButton = props => {
  return (
    <View style={styles.container}>
      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        style={{ marginRight: 10}}
      >
        {buttonItems.map((items, key) => {
          // console.log('kkkkk', items.navName);
          return (
            <TouchableOpacity
              onPress={() => props.onPress(items.navName)}
              activeOpacity={0.7}
              key={key}
              style={[styles.button, { backgroundColor: items.color }]}
            >
              <Text style={{ color: '#fff' }}>{items.name}</Text>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );
};

export default TopScrollButton;

const styles = StyleSheet.create({
  container: {
    height: 60,
    marginTop: 12
  },
  button: {
    paddingLeft: 10,
    paddingRight: 10,
    height: 45,
    width: 95,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10

    // #00bcd4, #4db6ac
  }
});
