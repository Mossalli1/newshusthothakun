import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Linking
} from 'react-native';

const CallTo = props => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.actionButton}
        onPress={() => props.callManager()}
      >
        <Text style={{ fontSize: 15 }}>MANAGER</Text>
      </TouchableOpacity>
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.actionButton}
        onPress={() => props.callDoctor()}
      >
        <Text style={{ fontSize: 15 }}>DOCTOR</Text>
      </TouchableOpacity>
    </View>
  );
};
export default CallTo;

const styles = StyleSheet.create({
  container: {
    marginTop: 130,
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: 200
  },
  actionButton: {
    backgroundColor: '#00acc1',
    width: 90,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 3
  }
});
