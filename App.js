// import { AppLoading, Notifications } from 'expo';
// import {AppLoading} from 'react-native-unimodules'
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import React, { useState, useEffect,  } from 'react';
import { Platform, StatusBar, StyleSheet, View , Text} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import _ from 'lodash';
//redux
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { persistor, store } from './redux/store';

import NotificationView from './components/NotificationView';
import AppNavigator from './navigation/AppNavigator';
import { AuthService } from './services/auth';
import { NotificationService } from './services/NotificationService';
import AlarmService from './services/AlarmService'
import AlarmScreen from './screens/profile/components/AlarmScreen';

import PushNotification from 'react-native-push-notification'
import PushNotificationIOS from "@react-native-community/push-notification-ios";
// import SplashScreen from 'react-native-splash-screen'


export const authService = new AuthService();
export const notificationService = new NotificationService();
// export const alarmService = new AlarmService();
// export const alarmService = new AlarmService();

const timeArray = [new Date(Date.now() + 10 * 1000), new Date(Date.now() + 10 * 5000)]



type Props = {};
class App extends React.Component<Props>{
  // const [isLoadingComplete, setLoadingComplete] = useState(false);
  // const [notification, setNotification] = useState({});
  constructor(props) {
    super(props);
    this.state = {
      notification: {}
    }
    // this.state = {
    //   senderId: appConfig.senderID
    // };

    this.notif = new AlarmService();
  }
  // const dateTime = new Date(Date.now() + 10 * 1000);

  // const timeArray = [new Date(Date.now() + 10 * 1000), new Date(Date.now() + 10 * 5000)]

  componentDidMount(){

    // SplashScreen.hide();
      
    // timeArray.map((t, i)=>{
    //   console.log('ttt',t);
    //   PushNotification.localNotificationSchedule({
    //     title: "My Notification Title", // (optional)
    //     message: "My Notification", // (required)
    //     date: t,
    //     soundName: 'alarm_clock_sound.caf',
    
    //     category: 'ff',
    //     userInfo: {id: '123', route: 'alarm', message: 'Time to Take Medicine', medicinName: ['napac', 'Rolac'] }
    //     // alertAction: <View><Text>tt</Text></View>
    //   });

    // })
    // this.notif.alarm()
    // this.notif._openAlarm()
    // alert('Hello')
    
    Platform.OS=='ios' && PushNotificationIOS.addEventListener(
      'localNotification',this._handleNotification
    );
    Platform.OS=='ios' && PushNotificationIOS.removeAllDeliveredNotifications();
    
    
    // this.notif._openAlarm()

    //   PushNotificationIOS.addEventListener(
  //     'localNotification',_handleNotification
  //   );


  }

  // useEffect(()=> {
  //   alarmService();
  //   // notificationService.registerForPushNotifications();
  //   // const _notificationSubscription = Notifications.addListener(_handleNotification);
  //   // const _notificationSubscription = PushNotificationIOS.addEventListener('Local',_handleNotification);
  //   // PushNotificationIOS.getScheduledLocalNotifications(_handleNotification);

  //   // _onLocalNotification();



  //   PushNotificationIOS.addEventListener(
  //     'localNotification',_handleNotification
  //   );



    // PushNotification.localNotificationSchedule({
    //   title: "My Notification Title", // (optional)
    //   message: "My Notification", // (required)
    //   date: dateTime,
    //   soundName: 'alarm_clock_sound.caf',
  
    //   category: 'ff',
    //   userInfo: {id: '123', route: 'alarm', message: 'Time to Take Medicine', medicinName: ['napa', 'Rolac'] }
    //   // alertAction: <View><Text>tt</Text></View>
    // });



    // alarm();




  //   this._subscribe();
    
  // return () => this._unsubscribe();

  

  //  console.log('Time are : ', [new Date(Date.now() + 10 * 1000),new Date(Date.now() + 10 * 2000)])

  // },[]);


  // console.log('ttt2 : ',props.fetchMedicine);




  // const alarm = () =>{
  //   timeArray.map((t, i)=>{
  //     console.log('ttt',t);
  //     PushNotification.localNotificationSchedule({
  //       title: "My Notification Title", // (optional)
  //       message: "My Notification", // (required)
  //       date: t,
  //       soundName: 'alarm_clock_sound.caf',
    
  //       category: 'ff',
  //       userInfo: {id: '123', route: 'alarm', message: 'Time to Take Medicine', medicinName: ['napac', 'Rolac'] }
  //       // alertAction: <View><Text>tt</Text></View>
  //     });

  //   })
    
  // }






  // console.log('Time are : ', new Date(Date.now() + 10 * 1000))




  _handleNotification = (notification) => {
      this.setState({notification: notification})
      console.log('Hello', notification)
  };


  

  // // console.log('PRINT IN %s=====>','App Not', !_.isEqual(notification, {}));
    

    

  // if (!isLoadingComplete && !props.skipLoadingScreen) {
  // //   return (
  // //     // null
  // //     <AppLoading
  // //       startAsync={loadResourcesAsync}
  // //       onError={handleLoadingError}
  // //       onFinish={() => handleFinishLoading(setLoadingComplete)}
  // //     />
  // //   );
  // // } else {
  //   ;
  // }
  render(){
    // console.log("State",this.state.notification)
    // PushNotificationIOS.removeAllDeliveredNotifications();

    if(!_.isEqual(this.state.notification, {})) {
      console.log('status....', !_.isEqual(this.state.notification, {}))
      if (this.state.notification._data.route==='alarm'){
        // console.log('----- notification fired ', notification);
        // return <View><Text>ttt</Text></View>
        return ( <AlarmScreen medicinName={this.state.notification._data.medicinName} yesButton={(value)=> this.setState({notification:{}})} laterButton={(value)=> this.setState({notification:{}})} stopButton={(value)=> this.setState({notification:{}})}  />
        // return ( <AlarmScreen medicinName={this.state.notification._data.medicinName} yesButton={(value)=> setNotification({})} laterButton={(value)=> setNotification({})} stopButton={(value)=> setNotification({})} />
        )
    }
      const {data={}}= this.state.notification;
      console.log('m:', data)
      const {message=null, route=null, medicinName=null } = data
      console.log("Data>>>>", data)
      if(route && route == 'notification'){
        // console.log('----- notification fired ', notification);
        return (
          <NotificationView message={message} setNotification={(value)=> setNotification(value)} />
        )
      }
      if(route && route == 'alarm'){
        // console.log('----- notification fired ', notification);
        return ( <AlarmScreen medicinName={medicinName} yesButton={(value)=> this.setState({notification:{}})} laterButton={(value)=> this.setState({notification:{}})} stopButton={(value)=> this.setState({notification:{}})} />
        )
      }
    }
    return (
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <AppNavigator />
          </PersistGate>
          {/* <AlarmService/> */}
        </Provider>
        
      </View>
    )
  }

onRegister(token) {
    Alert.alert("Registered !", JSON.stringify(token));
    console.log(token);
    this.setState({ registerToken: token.token, gcmRegistered: true });
  }
  
onNotif(notif) {
  // this.setState({notification: notif})
    console.log('My Not>>>>>',notif);
    alert('called');
  }
}

async function loadResourcesAsync() {
  await Promise.all([
    Asset.loadAsync([
      require('./assets/images/robot-dev.png'),
      require('./assets/images/robot-prod.png')
    ]),
    Font.loadAsync({
      // This is the font that we are using for our tab bar
      ...Ionicons.font,
      // We include SpaceMono because we use it in HomeScreen.js. Feel free to
      // remove this if you are not using it in your app
      'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf')
    })
  ]);
}

function handleLoadingError(error) {
  // In this case, you might want to report the error to your error reporting
  // service, for example Sentry
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}


export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  }
});
